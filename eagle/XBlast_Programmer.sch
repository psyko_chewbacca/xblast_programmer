<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.2.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="13" fill="1" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="11" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="14" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="no" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="no" active="yes"/>
<layer number="130" name="130bmp" color="2" fill="1" visible="no" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="145" name="DrillLegend_01-16" color="2" fill="9" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="231" name="Eagle3D_PG1" color="14" fill="1" visible="no" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="14" fill="2" visible="no" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="14" fill="4" visible="no" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="LPCModv1">
<packages>
<package name="M2X6">
<wire x1="-7.62" y1="-2.54" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="2.54" x2="7.62" y2="2.54" width="0.1524" layer="21"/>
<wire x1="7.62" y1="2.54" x2="7.62" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-2.54" x2="-5.08" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-2.54" x2="-7.62" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-2.54" x2="-5.08" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="-1.27" drill="0.8" shape="square"/>
<pad name="2" x="-6.35" y="1.27" drill="0.8"/>
<pad name="3" x="-3.81" y="-1.27" drill="0.8"/>
<pad name="4" x="-3.81" y="1.27" drill="0.8"/>
<pad name="5" x="-1.27" y="-1.27" drill="0.8"/>
<pad name="6" x="-1.27" y="1.27" drill="0.8"/>
<pad name="7" x="1.27" y="-1.27" drill="0.8"/>
<pad name="8" x="1.27" y="1.27" drill="0.8"/>
<pad name="9" x="3.81" y="-1.27" drill="0.8"/>
<pad name="10" x="3.81" y="1.27" drill="0.8"/>
<pad name="11" x="6.35" y="-1.27" drill="0.8"/>
<pad name="12" x="6.35" y="1.27" drill="0.8" first="yes"/>
<text x="-7.62" y="2.8575" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
</package>
<package name="SOIC-8W">
<wire x1="-2.5" y1="2.6755" x2="2.4" y2="2.6755" width="0.1524" layer="51"/>
<wire x1="2.4" y1="2.6755" x2="2.4" y2="-2.6755" width="0.1524" layer="21"/>
<wire x1="2.4" y1="-2.6755" x2="-2.5" y2="-2.6755" width="0.1524" layer="51"/>
<wire x1="-2.5" y1="-2.6755" x2="-2.5" y2="2.6755" width="0.1524" layer="21"/>
<circle x="-1.8" y="-1.7755" radius="0.2828" width="0.1524" layer="21"/>
<smd name="2" x="-0.635" y="-3.3755" dx="0.6" dy="2.2" layer="1"/>
<smd name="7" x="-0.635" y="3.4755" dx="0.6" dy="2.2" layer="1"/>
<smd name="1" x="-1.905" y="-3.3755" dx="0.6" dy="2.2" layer="1"/>
<smd name="3" x="0.635" y="-3.3755" dx="0.6" dy="2.2" layer="1"/>
<smd name="4" x="1.905" y="-3.3755" dx="0.6" dy="2.2" layer="1"/>
<smd name="8" x="-1.905" y="3.4755" dx="0.6" dy="2.2" layer="1"/>
<smd name="6" x="0.635" y="3.4755" dx="0.6" dy="2.2" layer="1"/>
<smd name="5" x="1.905" y="3.4755" dx="0.6" dy="2.2" layer="1"/>
<text x="-3.175" y="-2.6805" size="1.27" layer="25" font="vector" ratio="10" rot="R90">&gt;NAME</text>
<text x="4.445" y="-2.6805" size="1.27" layer="27" font="vector" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.1501" y1="-3.8756" x2="-1.6599" y2="-2.7755" layer="51"/>
<rectangle x1="-0.8801" y1="-3.8756" x2="-0.3899" y2="-2.7755" layer="51"/>
<rectangle x1="0.3899" y1="-3.8756" x2="0.8801" y2="-2.7755" layer="51"/>
<rectangle x1="1.6599" y1="-3.8756" x2="2.1501" y2="-2.7755" layer="51"/>
<rectangle x1="1.6599" y1="2.8755" x2="2.1501" y2="3.9756" layer="51"/>
<rectangle x1="0.3899" y1="2.8755" x2="0.8801" y2="3.9756" layer="51"/>
<rectangle x1="-0.8801" y1="2.8755" x2="-0.3899" y2="3.9756" layer="51"/>
<rectangle x1="-2.1501" y1="2.8755" x2="-1.6599" y2="3.9756" layer="51"/>
</package>
<package name="TIVA_BOOSTER_HEADER">
<wire x1="1.27" y1="-5.715" x2="0.635" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-6.35" x2="-1.27" y2="-5.715" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="1.27" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-3.175" x2="0.635" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-3.81" x2="-1.27" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-3.175" x2="-1.27" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-4.445" x2="0.635" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-3.81" x2="-1.27" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-5.715" x2="-1.27" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="1.27" y1="1.905" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="1.905" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="6.35" x2="1.27" y2="5.715" width="0.2032" layer="21"/>
<wire x1="1.27" y1="4.445" x2="0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="-1.27" y2="4.445" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="4.445" x2="-1.27" y2="5.715" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="5.715" x2="-0.635" y2="6.35" width="0.2032" layer="21"/>
<wire x1="1.27" y1="3.175" x2="0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="-1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="0.635" y1="8.89" x2="1.27" y2="8.255" width="0.2032" layer="21"/>
<wire x1="1.27" y1="6.985" x2="0.635" y2="6.35" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="6.35" x2="-1.27" y2="6.985" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="8.255" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="8.255" x2="-0.635" y2="8.89" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-8.255" x2="0.635" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-8.89" x2="-1.27" y2="-8.255" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-6.985" x2="0.635" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-6.35" x2="-1.27" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-8.255" x2="-1.27" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-10.795" x2="0.635" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-11.43" x2="-1.27" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-9.525" x2="0.635" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-8.89" x2="-1.27" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-10.795" x2="-1.27" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-13.335" x2="0.635" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-13.97" x2="-0.635" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-13.97" x2="-1.27" y2="-13.335" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-12.065" x2="0.635" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-11.43" x2="-1.27" y2="-12.065" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-13.335" x2="-1.27" y2="-12.065" width="0.2032" layer="21"/>
<pad name="1" x="0" y="10.16" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="0" y="7.62" drill="1.016" diameter="1.8796"/>
<pad name="3" x="0" y="5.08" drill="1.016" diameter="1.8796"/>
<pad name="4" x="0" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="5" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="0" y="-2.54" drill="1.016" diameter="1.8796"/>
<pad name="7" x="0" y="-5.08" drill="1.016" diameter="1.8796"/>
<pad name="8" x="0" y="-7.62" drill="1.016" diameter="1.8796"/>
<pad name="9" x="0" y="-10.16" drill="1.016" diameter="1.8796"/>
<pad name="10" x="0" y="-12.7" drill="1.016" diameter="1.8796"/>
<wire x1="-0.635" y1="8.89" x2="-1.27" y2="8.89" width="0.2286" layer="21"/>
<wire x1="-1.27" y1="8.89" x2="-1.27" y2="11.43" width="0.2286" layer="21"/>
<wire x1="-1.27" y1="11.43" x2="1.27" y2="11.43" width="0.2286" layer="21"/>
<wire x1="1.27" y1="8.89" x2="0.635" y2="8.89" width="0.2286" layer="21"/>
<wire x1="3.81" y1="-4.445" x2="3.81" y2="-5.715" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-5.715" x2="3.175" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-6.35" x2="1.27" y2="-5.715" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="3.81" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="3.81" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-3.175" x2="3.175" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-3.81" x2="1.27" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-3.175" x2="1.27" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-4.445" x2="3.175" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-3.81" x2="1.27" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-5.715" x2="1.27" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.175" x2="3.81" y2="1.905" width="0.2032" layer="21"/>
<wire x1="3.81" y1="1.905" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="1.27" y2="1.905" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="6.35" x2="3.81" y2="5.715" width="0.2032" layer="21"/>
<wire x1="3.81" y1="5.715" x2="3.81" y2="4.445" width="0.2032" layer="21"/>
<wire x1="3.81" y1="4.445" x2="3.175" y2="3.81" width="0.2032" layer="21"/>
<wire x1="1.905" y1="3.81" x2="1.27" y2="4.445" width="0.2032" layer="21"/>
<wire x1="1.27" y1="4.445" x2="1.27" y2="5.715" width="0.2032" layer="21"/>
<wire x1="1.27" y1="5.715" x2="1.905" y2="6.35" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.175" x2="3.175" y2="3.81" width="0.2032" layer="21"/>
<wire x1="1.905" y1="3.81" x2="1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="3.175" y1="8.89" x2="3.81" y2="8.255" width="0.2032" layer="21"/>
<wire x1="3.81" y1="8.255" x2="3.81" y2="6.985" width="0.2032" layer="21"/>
<wire x1="3.81" y1="6.985" x2="3.175" y2="6.35" width="0.2032" layer="21"/>
<wire x1="1.905" y1="6.35" x2="1.27" y2="6.985" width="0.2032" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="8.255" width="0.2032" layer="21"/>
<wire x1="1.27" y1="8.255" x2="1.905" y2="8.89" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-6.985" x2="3.81" y2="-8.255" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-8.255" x2="3.175" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-8.89" x2="1.27" y2="-8.255" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-6.985" x2="3.175" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-6.35" x2="1.27" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-8.255" x2="1.27" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-9.525" x2="3.81" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-10.795" x2="3.175" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-11.43" x2="1.27" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-9.525" x2="3.175" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-8.89" x2="1.27" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-10.795" x2="1.27" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-12.065" x2="3.81" y2="-13.335" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-13.335" x2="3.175" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-13.97" x2="1.905" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-13.97" x2="1.27" y2="-13.335" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-12.065" x2="3.175" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-11.43" x2="1.27" y2="-12.065" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-13.335" x2="1.27" y2="-12.065" width="0.2032" layer="21"/>
<pad name="21" x="2.54" y="10.16" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="22" x="2.54" y="7.62" drill="1.016" diameter="1.8796"/>
<pad name="23" x="2.54" y="5.08" drill="1.016" diameter="1.8796"/>
<pad name="24" x="2.54" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="25" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="26" x="2.54" y="-2.54" drill="1.016" diameter="1.8796"/>
<pad name="27" x="2.54" y="-5.08" drill="1.016" diameter="1.8796"/>
<pad name="28" x="2.54" y="-7.62" drill="1.016" diameter="1.8796"/>
<pad name="29" x="2.54" y="-10.16" drill="1.016" diameter="1.8796"/>
<pad name="30" x="2.54" y="-12.7" drill="1.016" diameter="1.8796"/>
<wire x1="1.905" y1="8.89" x2="1.27" y2="8.89" width="0.2286" layer="21"/>
<wire x1="1.27" y1="8.89" x2="1.27" y2="11.43" width="0.2286" layer="21"/>
<wire x1="1.27" y1="11.43" x2="3.81" y2="11.43" width="0.2286" layer="21"/>
<wire x1="3.81" y1="11.43" x2="3.81" y2="8.89" width="0.2286" layer="21"/>
<wire x1="3.81" y1="8.89" x2="3.175" y2="8.89" width="0.2286" layer="21"/>
<wire x1="44.45" y1="-5.715" x2="43.815" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-6.35" x2="41.91" y2="-5.715" width="0.2032" layer="21"/>
<wire x1="43.815" y1="-1.27" x2="44.45" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-3.175" x2="43.815" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-3.81" x2="41.91" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-3.175" x2="41.91" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-1.905" x2="42.545" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-4.445" x2="43.815" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-3.81" x2="41.91" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-5.715" x2="41.91" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="44.45" y1="1.905" x2="43.815" y2="1.27" width="0.2032" layer="21"/>
<wire x1="42.545" y1="1.27" x2="41.91" y2="1.905" width="0.2032" layer="21"/>
<wire x1="43.815" y1="1.27" x2="44.45" y2="0.635" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-0.635" x2="43.815" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-1.27" x2="41.91" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-0.635" x2="41.91" y2="0.635" width="0.2032" layer="21"/>
<wire x1="41.91" y1="0.635" x2="42.545" y2="1.27" width="0.2032" layer="21"/>
<wire x1="43.815" y1="6.35" x2="44.45" y2="5.715" width="0.2032" layer="21"/>
<wire x1="44.45" y1="4.445" x2="43.815" y2="3.81" width="0.2032" layer="21"/>
<wire x1="42.545" y1="3.81" x2="41.91" y2="4.445" width="0.2032" layer="21"/>
<wire x1="41.91" y1="4.445" x2="41.91" y2="5.715" width="0.2032" layer="21"/>
<wire x1="41.91" y1="5.715" x2="42.545" y2="6.35" width="0.2032" layer="21"/>
<wire x1="44.45" y1="3.175" x2="43.815" y2="3.81" width="0.2032" layer="21"/>
<wire x1="42.545" y1="3.81" x2="41.91" y2="3.175" width="0.2032" layer="21"/>
<wire x1="41.91" y1="1.905" x2="41.91" y2="3.175" width="0.2032" layer="21"/>
<wire x1="43.815" y1="8.89" x2="44.45" y2="8.255" width="0.2032" layer="21"/>
<wire x1="44.45" y1="6.985" x2="43.815" y2="6.35" width="0.2032" layer="21"/>
<wire x1="42.545" y1="6.35" x2="41.91" y2="6.985" width="0.2032" layer="21"/>
<wire x1="41.91" y1="6.985" x2="41.91" y2="8.255" width="0.2032" layer="21"/>
<wire x1="41.91" y1="8.255" x2="42.545" y2="8.89" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-8.255" x2="43.815" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-8.89" x2="41.91" y2="-8.255" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-6.985" x2="43.815" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-6.35" x2="41.91" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-8.255" x2="41.91" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-10.795" x2="43.815" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-11.43" x2="41.91" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-9.525" x2="43.815" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-8.89" x2="41.91" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-10.795" x2="41.91" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-13.335" x2="43.815" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="43.815" y1="-13.97" x2="42.545" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-13.97" x2="41.91" y2="-13.335" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-12.065" x2="43.815" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-11.43" x2="41.91" y2="-12.065" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-13.335" x2="41.91" y2="-12.065" width="0.2032" layer="21"/>
<pad name="40" x="43.18" y="10.16" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="39" x="43.18" y="7.62" drill="1.016" diameter="1.8796"/>
<pad name="38" x="43.18" y="5.08" drill="1.016" diameter="1.8796"/>
<pad name="37" x="43.18" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="36" x="43.18" y="0" drill="1.016" diameter="1.8796"/>
<pad name="35" x="43.18" y="-2.54" drill="1.016" diameter="1.8796"/>
<pad name="34" x="43.18" y="-5.08" drill="1.016" diameter="1.8796"/>
<pad name="33" x="43.18" y="-7.62" drill="1.016" diameter="1.8796"/>
<pad name="32" x="43.18" y="-10.16" drill="1.016" diameter="1.8796"/>
<pad name="31" x="43.18" y="-12.7" drill="1.016" diameter="1.8796"/>
<wire x1="42.545" y1="8.89" x2="41.91" y2="8.89" width="0.2286" layer="21"/>
<wire x1="41.91" y1="8.89" x2="41.91" y2="11.43" width="0.2286" layer="21"/>
<wire x1="41.91" y1="11.43" x2="44.45" y2="11.43" width="0.2286" layer="21"/>
<wire x1="44.45" y1="8.89" x2="43.815" y2="8.89" width="0.2286" layer="21"/>
<wire x1="46.99" y1="-4.445" x2="46.99" y2="-5.715" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-5.715" x2="46.355" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-6.35" x2="44.45" y2="-5.715" width="0.2032" layer="21"/>
<wire x1="46.355" y1="-1.27" x2="46.99" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-1.905" x2="46.99" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-3.175" x2="46.355" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-3.81" x2="44.45" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-3.175" x2="44.45" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-1.905" x2="45.085" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-4.445" x2="46.355" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-3.81" x2="44.45" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-5.715" x2="44.45" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="46.99" y1="3.175" x2="46.99" y2="1.905" width="0.2032" layer="21"/>
<wire x1="46.99" y1="1.905" x2="46.355" y2="1.27" width="0.2032" layer="21"/>
<wire x1="45.085" y1="1.27" x2="44.45" y2="1.905" width="0.2032" layer="21"/>
<wire x1="46.355" y1="1.27" x2="46.99" y2="0.635" width="0.2032" layer="21"/>
<wire x1="46.99" y1="0.635" x2="46.99" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-0.635" x2="46.355" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-1.27" x2="44.45" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-0.635" x2="44.45" y2="0.635" width="0.2032" layer="21"/>
<wire x1="44.45" y1="0.635" x2="45.085" y2="1.27" width="0.2032" layer="21"/>
<wire x1="46.355" y1="6.35" x2="46.99" y2="5.715" width="0.2032" layer="21"/>
<wire x1="46.99" y1="5.715" x2="46.99" y2="4.445" width="0.2032" layer="21"/>
<wire x1="46.99" y1="4.445" x2="46.355" y2="3.81" width="0.2032" layer="21"/>
<wire x1="45.085" y1="3.81" x2="44.45" y2="4.445" width="0.2032" layer="21"/>
<wire x1="44.45" y1="4.445" x2="44.45" y2="5.715" width="0.2032" layer="21"/>
<wire x1="44.45" y1="5.715" x2="45.085" y2="6.35" width="0.2032" layer="21"/>
<wire x1="46.99" y1="3.175" x2="46.355" y2="3.81" width="0.2032" layer="21"/>
<wire x1="45.085" y1="3.81" x2="44.45" y2="3.175" width="0.2032" layer="21"/>
<wire x1="44.45" y1="1.905" x2="44.45" y2="3.175" width="0.2032" layer="21"/>
<wire x1="46.355" y1="8.89" x2="46.99" y2="8.255" width="0.2032" layer="21"/>
<wire x1="46.99" y1="8.255" x2="46.99" y2="6.985" width="0.2032" layer="21"/>
<wire x1="46.99" y1="6.985" x2="46.355" y2="6.35" width="0.2032" layer="21"/>
<wire x1="45.085" y1="6.35" x2="44.45" y2="6.985" width="0.2032" layer="21"/>
<wire x1="44.45" y1="6.985" x2="44.45" y2="8.255" width="0.2032" layer="21"/>
<wire x1="44.45" y1="8.255" x2="45.085" y2="8.89" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-6.985" x2="46.99" y2="-8.255" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-8.255" x2="46.355" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-8.89" x2="44.45" y2="-8.255" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-6.985" x2="46.355" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-6.35" x2="44.45" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-8.255" x2="44.45" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-9.525" x2="46.99" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-10.795" x2="46.355" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-11.43" x2="44.45" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-9.525" x2="46.355" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-8.89" x2="44.45" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-10.795" x2="44.45" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-12.065" x2="46.99" y2="-13.335" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-13.335" x2="46.355" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="46.355" y1="-13.97" x2="45.085" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-13.97" x2="44.45" y2="-13.335" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-12.065" x2="46.355" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-11.43" x2="44.45" y2="-12.065" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-13.335" x2="44.45" y2="-12.065" width="0.2032" layer="21"/>
<pad name="20" x="45.72" y="10.16" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="19" x="45.72" y="7.62" drill="1.016" diameter="1.8796"/>
<pad name="18" x="45.72" y="5.08" drill="1.016" diameter="1.8796"/>
<pad name="17" x="45.72" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="16" x="45.72" y="0" drill="1.016" diameter="1.8796"/>
<pad name="15" x="45.72" y="-2.54" drill="1.016" diameter="1.8796"/>
<pad name="14" x="45.72" y="-5.08" drill="1.016" diameter="1.8796"/>
<pad name="13" x="45.72" y="-7.62" drill="1.016" diameter="1.8796"/>
<pad name="12" x="45.72" y="-10.16" drill="1.016" diameter="1.8796"/>
<pad name="11" x="45.72" y="-12.7" drill="1.016" diameter="1.8796"/>
<wire x1="45.085" y1="8.89" x2="44.45" y2="8.89" width="0.2286" layer="21"/>
<wire x1="44.45" y1="8.89" x2="44.45" y2="11.43" width="0.2286" layer="21"/>
<wire x1="44.45" y1="11.43" x2="46.99" y2="11.43" width="0.2286" layer="21"/>
<wire x1="46.99" y1="11.43" x2="46.99" y2="8.89" width="0.2286" layer="21"/>
<wire x1="46.99" y1="8.89" x2="46.355" y2="8.89" width="0.2286" layer="21"/>
</package>
<package name="DO323">
<wire x1="-1" y1="0.7" x2="1" y2="0.7" width="0.1524" layer="21"/>
<wire x1="1" y1="0.7" x2="1" y2="-0.7" width="0.1524" layer="51"/>
<wire x1="1" y1="-0.7" x2="-1" y2="-0.7" width="0.1524" layer="21"/>
<wire x1="-1" y1="-0.7" x2="-1" y2="0.7" width="0.1524" layer="51"/>
<wire x1="-0.5" y1="0" x2="0.1" y2="0.4" width="0.1524" layer="21"/>
<wire x1="0.1" y1="0.4" x2="0.1" y2="0" width="0.1524" layer="21"/>
<wire x1="0.1" y1="0" x2="0.1" y2="-0.4" width="0.1524" layer="21"/>
<wire x1="0.1" y1="-0.4" x2="-0.5" y2="0" width="0.1524" layer="21"/>
<smd name="C" x="-1.1" y="0" dx="0.6" dy="0.6" layer="1"/>
<smd name="A" x="1.1" y="0" dx="0.6" dy="0.6" layer="1"/>
<text x="-2.54" y="0.9525" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.2225" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.7" y1="-0.7" x2="-0.5" y2="0.7" layer="21"/>
<wire x1="0.1" y1="0" x2="0.508" y2="0" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="HEADER2X6">
<wire x1="-3.81" y1="10.16" x2="6.35" y2="10.16" width="0.4064" layer="94"/>
<wire x1="6.35" y1="10.16" x2="6.35" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="6.35" y1="-7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="-7.62" x2="-3.81" y2="10.16" width="0.4064" layer="94"/>
<text x="-3.81" y="11.43" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="7" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="9" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="10" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="11" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="12" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
<symbol name="SST26VF032">
<wire x1="-10.16" y1="10.16" x2="-10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<pin name="CE#" x="-15.24" y="7.62" length="middle" direction="in"/>
<pin name="SO/SIO1" x="-15.24" y="2.54" length="middle"/>
<pin name="SIO2" x="-15.24" y="-2.54" length="middle"/>
<pin name="VSS" x="-15.24" y="-7.62" length="middle" direction="pwr"/>
<pin name="SI/SIO0" x="15.24" y="-7.62" length="middle" rot="R180"/>
<pin name="SCK" x="15.24" y="-2.54" length="middle" direction="in" rot="R180"/>
<pin name="SIO3" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="VDD" x="15.24" y="7.62" length="middle" direction="pwr" rot="R180"/>
</symbol>
<symbol name="4X10_TIVA_BOOSTER_HEADER">
<wire x1="-33.02" y1="-17.78" x2="-33.02" y2="10.16" width="0.254" layer="94"/>
<wire x1="-33.02" y1="10.16" x2="-27.94" y2="10.16" width="0.254" layer="94"/>
<wire x1="-27.94" y1="10.16" x2="-27.94" y2="-17.78" width="0.254" layer="94"/>
<wire x1="-27.94" y1="-17.78" x2="-33.02" y2="-17.78" width="0.254" layer="94"/>
<pin name="P$1" x="-25.4" y="7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$2" x="-25.4" y="5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$3" x="-25.4" y="2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$4" x="-25.4" y="0" visible="pad" length="short" rot="R180"/>
<pin name="P$5" x="-25.4" y="-2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$6" x="-25.4" y="-5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$7" x="-25.4" y="-7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$8" x="-25.4" y="-10.16" visible="pad" length="short" rot="R180"/>
<pin name="P$9" x="-25.4" y="-12.7" visible="pad" length="short" rot="R180"/>
<pin name="P$10" x="-25.4" y="-15.24" visible="pad" length="short" rot="R180"/>
<wire x1="22.86" y1="-17.78" x2="22.86" y2="10.16" width="0.254" layer="94"/>
<wire x1="22.86" y1="10.16" x2="27.94" y2="10.16" width="0.254" layer="94"/>
<wire x1="27.94" y1="10.16" x2="27.94" y2="-17.78" width="0.254" layer="94"/>
<wire x1="27.94" y1="-17.78" x2="22.86" y2="-17.78" width="0.254" layer="94"/>
<pin name="P$20" x="30.48" y="7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$19" x="30.48" y="5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$18" x="30.48" y="2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$17" x="30.48" y="0" visible="pad" length="short" rot="R180"/>
<pin name="P$16" x="30.48" y="-2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$15" x="30.48" y="-5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$14" x="30.48" y="-7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$13" x="30.48" y="-10.16" visible="pad" length="short" rot="R180"/>
<pin name="P$12" x="30.48" y="-12.7" visible="pad" length="short" rot="R180"/>
<pin name="P$11" x="30.48" y="-15.24" visible="pad" length="short" rot="R180"/>
<wire x1="-17.78" y1="-17.78" x2="-17.78" y2="10.16" width="0.254" layer="94"/>
<wire x1="-17.78" y1="10.16" x2="-12.7" y2="10.16" width="0.254" layer="94"/>
<wire x1="-12.7" y1="10.16" x2="-12.7" y2="-17.78" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-17.78" x2="-17.78" y2="-17.78" width="0.254" layer="94"/>
<pin name="P$21" x="-10.16" y="7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$22" x="-10.16" y="5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$23" x="-10.16" y="2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$24" x="-10.16" y="0" visible="pad" length="short" rot="R180"/>
<pin name="P$25" x="-10.16" y="-2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$26" x="-10.16" y="-5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$27" x="-10.16" y="-7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$28" x="-10.16" y="-10.16" visible="pad" length="short" rot="R180"/>
<pin name="P$29" x="-10.16" y="-12.7" visible="pad" length="short" rot="R180"/>
<pin name="P$30" x="-10.16" y="-15.24" visible="pad" length="short" rot="R180"/>
<wire x1="7.62" y1="-17.78" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="12.7" y2="10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="10.16" x2="12.7" y2="-17.78" width="0.254" layer="94"/>
<wire x1="12.7" y1="-17.78" x2="7.62" y2="-17.78" width="0.254" layer="94"/>
<pin name="P$40" x="15.24" y="7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$39" x="15.24" y="5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$38" x="15.24" y="2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$37" x="15.24" y="0" visible="pad" length="short" rot="R180"/>
<pin name="P$36" x="15.24" y="-2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$35" x="15.24" y="-5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$34" x="15.24" y="-7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$33" x="15.24" y="-10.16" visible="pad" length="short" rot="R180"/>
<pin name="P$32" x="15.24" y="-12.7" visible="pad" length="short" rot="R180"/>
<pin name="P$31" x="15.24" y="-15.24" visible="pad" length="short" rot="R180"/>
<text x="-33.02" y="12.7" size="2.54" layer="95" font="vector">J1</text>
<text x="22.86" y="12.7" size="2.54" layer="95" font="vector">J2</text>
<text x="-17.78" y="12.7" size="2.54" layer="95" font="vector">J3</text>
<text x="7.62" y="12.7" size="2.54" layer="95" font="vector">J4</text>
</symbol>
<symbol name="DIODE">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="2.54" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.3114" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CON_LPC_MIN" prefix="J">
<gates>
<gate name="G$1" symbol="HEADER2X6" x="0" y="0"/>
</gates>
<devices>
<device name="" package="M2X6">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SST26VF032" prefix="U">
<gates>
<gate name="G$1" symbol="SST26VF032" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOIC-8W">
<connects>
<connect gate="G$1" pin="CE#" pad="1"/>
<connect gate="G$1" pin="SCK" pad="6"/>
<connect gate="G$1" pin="SI/SIO0" pad="5"/>
<connect gate="G$1" pin="SIO2" pad="3"/>
<connect gate="G$1" pin="SIO3" pad="7"/>
<connect gate="G$1" pin="SO/SIO1" pad="2"/>
<connect gate="G$1" pin="VDD" pad="8"/>
<connect gate="G$1" pin="VSS" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TIVA_BOOSTER_HEADERS_4X10" prefix="A">
<gates>
<gate name="G$1" symbol="4X10_TIVA_BOOSTER_HEADER" x="30.48" y="2.54"/>
</gates>
<devices>
<device name="" package="TIVA_BOOSTER_HEADER">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$10" pad="10"/>
<connect gate="G$1" pin="P$11" pad="11"/>
<connect gate="G$1" pin="P$12" pad="12"/>
<connect gate="G$1" pin="P$13" pad="13"/>
<connect gate="G$1" pin="P$14" pad="14"/>
<connect gate="G$1" pin="P$15" pad="15"/>
<connect gate="G$1" pin="P$16" pad="16"/>
<connect gate="G$1" pin="P$17" pad="17"/>
<connect gate="G$1" pin="P$18" pad="18"/>
<connect gate="G$1" pin="P$19" pad="19"/>
<connect gate="G$1" pin="P$2" pad="2"/>
<connect gate="G$1" pin="P$20" pad="20"/>
<connect gate="G$1" pin="P$21" pad="21"/>
<connect gate="G$1" pin="P$22" pad="22"/>
<connect gate="G$1" pin="P$23" pad="23"/>
<connect gate="G$1" pin="P$24" pad="24"/>
<connect gate="G$1" pin="P$25" pad="25"/>
<connect gate="G$1" pin="P$26" pad="26"/>
<connect gate="G$1" pin="P$27" pad="27"/>
<connect gate="G$1" pin="P$28" pad="28"/>
<connect gate="G$1" pin="P$29" pad="29"/>
<connect gate="G$1" pin="P$3" pad="3"/>
<connect gate="G$1" pin="P$30" pad="30"/>
<connect gate="G$1" pin="P$31" pad="31"/>
<connect gate="G$1" pin="P$32" pad="32"/>
<connect gate="G$1" pin="P$33" pad="33"/>
<connect gate="G$1" pin="P$34" pad="34"/>
<connect gate="G$1" pin="P$35" pad="35"/>
<connect gate="G$1" pin="P$36" pad="36"/>
<connect gate="G$1" pin="P$37" pad="37"/>
<connect gate="G$1" pin="P$38" pad="38"/>
<connect gate="G$1" pin="P$39" pad="39"/>
<connect gate="G$1" pin="P$4" pad="4"/>
<connect gate="G$1" pin="P$40" pad="40"/>
<connect gate="G$1" pin="P$5" pad="5"/>
<connect gate="G$1" pin="P$6" pad="6"/>
<connect gate="G$1" pin="P$7" pad="7"/>
<connect gate="G$1" pin="P$8" pad="8"/>
<connect gate="G$1" pin="P$9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DO323-DIODE" prefix="D">
<gates>
<gate name="G$1" symbol="DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DO323">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="dp_devices">
<description>Dangerous Prototypes Standard PCB sizes
http://dangerousprototypes.com</description>
<packages>
<package name="R805">
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.762" y="1.016" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-0.762" y="-2.286" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
</package>
<package name="R603">
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.762" y="1.016" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-0.762" y="-2.286" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
</package>
<package name="R402">
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.762" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.032" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
</package>
<package name="R1206">
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.397" y="1.143" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-1.397" y="-2.413" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
</package>
<package name="RTH025W">
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.8575" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="TO-220-2">
<wire x1="4.826" y1="-1.778" x2="5.08" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-1.778" x2="-4.826" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.524" x2="-4.826" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.397" x2="5.08" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.524" x2="-5.08" y2="1.397" width="0.1524" layer="21"/>
<circle x="-4.6228" y="-1.1684" radius="0.254" width="0" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-5.08" y="-3.3782" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<text x="-1.27" y="-1.27" size="1.27" layer="51" font="vector" ratio="10">1</text>
<text x="3.81" y="-1.27" size="1.27" layer="51" font="vector" ratio="10">2</text>
<rectangle x1="-5.334" y1="1.27" x2="-3.429" y2="2.54" layer="21"/>
<rectangle x1="-3.429" y1="1.778" x2="-1.651" y2="2.54" layer="21"/>
<rectangle x1="-1.651" y1="1.27" x2="-0.889" y2="2.54" layer="21"/>
<rectangle x1="-0.889" y1="1.778" x2="0.889" y2="2.54" layer="21"/>
<rectangle x1="0.889" y1="1.27" x2="1.651" y2="2.54" layer="21"/>
<rectangle x1="1.651" y1="1.778" x2="3.429" y2="2.54" layer="21"/>
<rectangle x1="3.429" y1="1.27" x2="5.334" y2="2.54" layer="21"/>
<rectangle x1="-3.429" y1="1.27" x2="-1.651" y2="1.778" layer="51"/>
<rectangle x1="-0.889" y1="1.27" x2="0.889" y2="1.778" layer="21"/>
<rectangle x1="1.651" y1="1.27" x2="3.429" y2="1.778" layer="51"/>
</package>
<package name="JUMPER_SOLDER_1X2">
<description>Solder jumper 6mil clearance</description>
<wire x1="-1.476" y1="1" x2="-1.476" y2="-1" width="0.3048" layer="21"/>
<wire x1="-1.476" y1="-1" x2="-0.0191" y2="-1" width="0.3048" layer="21"/>
<wire x1="-0.0191" y1="-1" x2="1.476" y2="-1" width="0.3048" layer="21"/>
<wire x1="1.476" y1="-1" x2="1.476" y2="1" width="0.3048" layer="21"/>
<wire x1="1.476" y1="1" x2="-0.0191" y2="1" width="0.3048" layer="21"/>
<wire x1="-0.0191" y1="1" x2="-1.476" y2="1" width="0.3048" layer="21"/>
<wire x1="-0.0191" y1="-1" x2="-0.0191" y2="-0.75" width="0.3048" layer="21"/>
<wire x1="-0.0191" y1="1" x2="-0.0191" y2="0.75" width="0.3048" layer="21"/>
<smd name="1" x="-0.6604" y="0" dx="1.4224" dy="1.143" layer="1" rot="R90" cream="no"/>
<smd name="2" x="0.6604" y="0" dx="1.4224" dy="1.143" layer="1" rot="R90" cream="no"/>
<text x="-1.425" y="1.425" size="1.27" layer="25" font="vector" ratio="10" rot="SR0">&gt;NAME</text>
</package>
<package name="PTC0805">
<description>0805 fuse</description>
<wire x1="-0.5" y1="0.75" x2="0.5" y2="0.75" width="0.0508" layer="21"/>
<wire x1="0.5" y1="-0.75" x2="-0.5" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="0.5" y1="0.75" x2="0.55" y2="0.75" width="0.0508" layer="49"/>
<wire x1="0.55" y1="0.75" x2="0.55" y2="-0.75" width="0.0508" layer="49"/>
<wire x1="0.55" y1="-0.75" x2="0.5" y2="-0.75" width="0.0508" layer="49"/>
<wire x1="-0.5" y1="0.75" x2="-0.55" y2="0.75" width="0.0508" layer="49"/>
<wire x1="-0.55" y1="0.75" x2="-0.55" y2="-0.75" width="0.0508" layer="49"/>
<wire x1="-0.55" y1="-0.75" x2="-0.5" y2="-0.75" width="0.0508" layer="49"/>
<wire x1="0.55" y1="0.75" x2="1.1" y2="0.75" width="0.0508" layer="49"/>
<wire x1="1.1" y1="0.75" x2="1.1" y2="0.45" width="0.0508" layer="49"/>
<wire x1="0.55" y1="-0.75" x2="1.1" y2="-0.75" width="0.0508" layer="49"/>
<wire x1="1.1" y1="-0.75" x2="1.1" y2="-0.45" width="0.0508" layer="49"/>
<wire x1="-0.55" y1="-0.75" x2="-1.1" y2="-0.75" width="0.0508" layer="49"/>
<wire x1="-1.1" y1="-0.75" x2="-1.1" y2="-0.45" width="0.0508" layer="49"/>
<wire x1="-0.55" y1="0.75" x2="-1.1" y2="0.75" width="0.0508" layer="49"/>
<wire x1="-1.1" y1="0.75" x2="-1.1" y2="0.45" width="0.0508" layer="49"/>
<wire x1="1.1" y1="0.45" x2="1.1" y2="-0.45" width="0.0508" layer="49" curve="180"/>
<wire x1="-1.1" y1="-0.45" x2="-1.1" y2="0.45" width="0.0508" layer="49" curve="180"/>
<smd name="1" x="-1.1" y="0" dx="1" dy="1.5" layer="1"/>
<smd name="2" x="1.1" y="0" dx="1" dy="1.5" layer="1"/>
<text x="-2.54" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<polygon width="0" layer="49">
<vertex x="0.55" y="0.75"/>
<vertex x="1.1" y="0.75"/>
<vertex x="1.1" y="0.45"/>
<vertex x="0.55" y="0.45"/>
</polygon>
<polygon width="0" layer="49">
<vertex x="0.55" y="0.45"/>
<vertex x="0.95" y="0.45"/>
<vertex x="0.9" y="0.4"/>
<vertex x="0.85" y="0.4"/>
<vertex x="0.8" y="0.35"/>
<vertex x="0.75" y="0.3"/>
<vertex x="0.7" y="0.25"/>
<vertex x="0.7" y="0.2"/>
<vertex x="0.7" y="0.15"/>
<vertex x="0.65" y="0.1"/>
<vertex x="0.65" y="-0.1"/>
<vertex x="0.7" y="-0.15"/>
<vertex x="0.7" y="-0.2"/>
<vertex x="0.75" y="-0.25"/>
<vertex x="0.75" y="-0.3"/>
<vertex x="0.8" y="-0.35"/>
<vertex x="0.85" y="-0.4"/>
<vertex x="0.9" y="-0.4"/>
<vertex x="0.95" y="-0.4"/>
<vertex x="1" y="-0.45"/>
<vertex x="1.05" y="-0.45"/>
<vertex x="1.1" y="-0.45"/>
<vertex x="1.1" y="-0.75"/>
<vertex x="0.55" y="-0.75"/>
</polygon>
<polygon width="0" layer="49">
<vertex x="-0.55" y="0.75"/>
<vertex x="-1.1" y="0.75"/>
<vertex x="-1.1" y="0.45"/>
<vertex x="-1.05" y="0.45"/>
<vertex x="-1" y="0.45"/>
<vertex x="-0.95" y="0.4"/>
<vertex x="-0.85" y="0.4"/>
<vertex x="-0.75" y="0.3"/>
<vertex x="-0.7" y="0.25"/>
<vertex x="-0.7" y="0.2"/>
<vertex x="-0.65" y="0.15"/>
<vertex x="-0.7" y="0.15"/>
<vertex x="-0.65" y="0.1"/>
<vertex x="-0.65" y="-0.1"/>
<vertex x="-0.7" y="-0.15"/>
<vertex x="-0.7" y="-0.2"/>
<vertex x="-0.75" y="-0.25"/>
<vertex x="-0.75" y="-0.3"/>
<vertex x="-0.8" y="-0.35"/>
<vertex x="-0.85" y="-0.35"/>
<vertex x="-0.9" y="-0.4"/>
<vertex x="-0.95" y="-0.4"/>
<vertex x="-1" y="-0.45"/>
<vertex x="-1.1" y="-0.45"/>
<vertex x="-1.1" y="-0.75"/>
<vertex x="-0.55" y="-0.75"/>
</polygon>
</package>
<package name="PTC1812L">
<description>4.73mm x 3.41mm</description>
<wire x1="-1.165" y1="1.705" x2="1.165" y2="1.705" width="0.1524" layer="21"/>
<wire x1="1.165" y1="1.705" x2="2.365" y2="1.705" width="0.0634" layer="51"/>
<wire x1="2.365" y1="1.705" x2="2.365" y2="0.5" width="0.0634" layer="51"/>
<wire x1="2.365" y1="-0.5" x2="2.365" y2="-1.705" width="0.0634" layer="51"/>
<wire x1="2.365" y1="-1.705" x2="1.165" y2="-1.705" width="0.0634" layer="51"/>
<wire x1="1.165" y1="-1.705" x2="-1.165" y2="-1.705" width="0.1524" layer="21"/>
<wire x1="-1.165" y1="-1.705" x2="-2.365" y2="-1.705" width="0.0634" layer="51"/>
<wire x1="-2.365" y1="-1.705" x2="-2.365" y2="-0.5" width="0.0634" layer="51"/>
<wire x1="-2.365" y1="0.5" x2="-2.365" y2="1.705" width="0.0634" layer="51"/>
<wire x1="-2.365" y1="-0.5" x2="-2.365" y2="0.5" width="0.0634" layer="51" curve="180"/>
<wire x1="2.365" y1="-0.5" x2="2.365" y2="0.5" width="0.0634" layer="51" curve="-180"/>
<wire x1="-1.165" y1="1.705" x2="-1.165" y2="-1.705" width="0.1524" layer="21"/>
<wire x1="1.165" y1="1.705" x2="1.165" y2="-1.705" width="0.1524" layer="21"/>
<smd name="1" x="-2.62" y="0" dx="1.7" dy="3.15" layer="1"/>
<smd name="2" x="2.62" y="0" dx="1.7" dy="3.15" layer="1"/>
<text x="-3.81" y="2.54" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-3.81" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<polygon width="0.1524" layer="51">
<vertex x="-2.365" y="1.705"/>
<vertex x="-1.165" y="1.705"/>
<vertex x="-1.165" y="-1.705"/>
<vertex x="-2.365" y="-1.705"/>
<vertex x="-2.365" y="-0.5" curve="90"/>
<vertex x="-1.87" y="-0.005"/>
<vertex x="-1.87" y="0"/>
<vertex x="-1.87" y="0.005" curve="90"/>
<vertex x="-2.365" y="0.5"/>
</polygon>
<polygon width="0.1524" layer="51">
<vertex x="2.365" y="1.705"/>
<vertex x="2.365" y="0.5" curve="90"/>
<vertex x="1.87" y="0.005"/>
<vertex x="1.87" y="0"/>
<vertex x="1.87" y="-0.005" curve="90"/>
<vertex x="2.365" y="-0.5"/>
<vertex x="2.365" y="-1.705"/>
<vertex x="1.165" y="-1.705"/>
<vertex x="1.165" y="1.705"/>
</polygon>
</package>
<package name="LED-3MM">
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="-1.524" y1="0" x2="-1.1708" y2="0.9756" width="0.1524" layer="51" curve="-39.80361"/>
<wire x1="-1.524" y1="0" x2="-1.1391" y2="-1.0125" width="0.1524" layer="51" curve="41.633208"/>
<wire x1="1.1571" y1="0.9918" x2="1.524" y2="0" width="0.1524" layer="51" curve="-40.601165"/>
<wire x1="1.1708" y1="-0.9756" x2="1.524" y2="0" width="0.1524" layer="51" curve="39.80361"/>
<wire x1="0" y1="1.524" x2="1.2401" y2="0.8858" width="0.1524" layer="21" curve="-54.461337"/>
<wire x1="-1.2192" y1="0.9144" x2="0" y2="1.524" width="0.1524" layer="21" curve="-53.130102"/>
<wire x1="0" y1="-1.524" x2="1.203" y2="-0.9356" width="0.1524" layer="21" curve="52.126876"/>
<wire x1="-1.203" y1="-0.9356" x2="0" y2="-1.524" width="0.1524" layer="21" curve="52.126876"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="1.905" y="0.381" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
</package>
<package name="LED-5MM">
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED-805">
<wire x1="-0.35" y1="0.925" x2="0.35" y2="0.925" width="0.1016" layer="51" curve="162.394521"/>
<wire x1="-0.35" y1="-0.925" x2="0.35" y2="-0.925" width="0.1016" layer="51" curve="-162.394521"/>
<wire x1="0.575" y1="0.525" x2="0.575" y2="-0.525" width="0.1016" layer="51"/>
<wire x1="-0.575" y1="-0.5" x2="-0.575" y2="0.925" width="0.1016" layer="51"/>
<circle x="-0.45" y="0.85" radius="0.103" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.05" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="0" y="-1.05" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="0.3" y1="0.5" x2="0.625" y2="1" layer="51"/>
<rectangle x1="-0.325" y1="0.5" x2="-0.175" y2="0.75" layer="51"/>
<rectangle x1="0.175" y1="0.5" x2="0.325" y2="0.75" layer="51"/>
<rectangle x1="-0.2" y1="0.5" x2="0.2" y2="0.675" layer="51"/>
<rectangle x1="0.3" y1="-1" x2="0.625" y2="-0.5" layer="51"/>
<rectangle x1="-0.625" y1="-1" x2="-0.3" y2="-0.5" layer="51"/>
<rectangle x1="0.175" y1="-0.75" x2="0.325" y2="-0.5" layer="51"/>
<rectangle x1="-0.325" y1="-0.75" x2="-0.175" y2="-0.5" layer="51"/>
<rectangle x1="-0.2" y1="-0.675" x2="0.2" y2="-0.5" layer="51"/>
<rectangle x1="-0.1" y1="0" x2="0.1" y2="0.2" layer="21"/>
<rectangle x1="-0.6" y1="0.5" x2="-0.3" y2="0.8" layer="51"/>
<rectangle x1="-0.625" y1="0.925" x2="-0.3" y2="1" layer="51"/>
</package>
<package name="LED-5MM-90DEG">
<wire x1="-2.55" y1="2.5" x2="2.65" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-2.55" y1="-2.5" x2="2.65" y2="-2.5" width="0.1524" layer="21"/>
<wire x1="2.65" y1="-2.5" x2="3.65" y2="-2.5" width="0.1524" layer="21"/>
<wire x1="2.65" y1="2.9" x2="2.65" y2="-2.5" width="0.1524" layer="21"/>
<wire x1="3.65" y1="2.9" x2="3.65" y2="-2.5" width="0.1524" layer="21"/>
<wire x1="2.65" y1="2.9" x2="3.65" y2="2.9" width="0.1524" layer="21"/>
<wire x1="-2.55" y1="2.5" x2="-2.55" y2="-2.5" width="0.1524" layer="21" curve="180"/>
<pad name="A" x="5.1" y="1.25" drill="0.8128" shape="octagon"/>
<pad name="K" x="5.1" y="-1.25" drill="0.8128" shape="octagon"/>
<text x="-1.778" y="3.302" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-4.5" y="-0.5" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<text x="4.572" y="-3.556" size="1.016" layer="21" ratio="10">K</text>
<rectangle x1="3.65" y1="-1.5" x2="4.35" y2="-1" layer="21"/>
<rectangle x1="3.65" y1="1" x2="4.35" y2="1.5" layer="21"/>
</package>
<package name="LED-603">
<wire x1="-0.3" y1="0.8" x2="0.3" y2="0.8" width="0.1016" layer="51" curve="170.055574"/>
<wire x1="-0.275" y1="-0.825" x2="0.275" y2="-0.825" width="0.0508" layer="51" curve="-180"/>
<wire x1="-0.4" y1="0.375" x2="-0.4" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.35" x2="0.4" y2="-0.35" width="0.1016" layer="51"/>
<circle x="-0.35" y="0.625" radius="0.075" width="0.0508" layer="51"/>
<smd name="C" x="0" y="0.75" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.8" layer="1"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.7" x2="-0.25" y2="0.85" layer="51"/>
<rectangle x1="-0.275" y1="0.55" x2="-0.225" y2="0.6" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="-0.4" y2="0.725" layer="51"/>
<rectangle x1="0.25" y1="0.55" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="0.45" y2="0.575" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="-0.25" y2="-0.35" layer="51"/>
<rectangle x1="0.25" y1="-0.85" x2="0.45" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.575" x2="0.275" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.65" x2="-0.175" y2="-0.55" layer="51"/>
<rectangle x1="0.175" y1="-0.65" x2="0.275" y2="-0.55" layer="51"/>
<rectangle x1="-0.125" y1="0" x2="0.125" y2="0.25" layer="21"/>
</package>
<package name="C805">
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.127" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.127" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.016" layer="25" ratio="12">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.016" layer="27" ratio="12">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
</package>
<package name="C402">
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
</package>
<package name="C603">
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8302" y2="0.4801" layer="51" rot="R180"/>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
</package>
<package name="C1206">
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
</package>
<package name="C0.1PTH">
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="1.27" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-1.27" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<text x="-2.54" y="1.5875" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.8575" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="SUPPLY_GND">
<wire x1="1.27" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.524" y="-2.032" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="SUPPLY_+3V3">
<wire x1="1.27" y1="0.635" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="0.635" width="0.254" layer="94"/>
<text x="0" y="2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="+3V3" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="JUMP_SOLDER_1X2">
<wire x1="-2.54" y1="0" x2="-1.524" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="1.524" y2="0" width="0.1524" layer="94"/>
<text x="-4.318" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<polygon width="0.254" layer="94">
<vertex x="-0.635" y="0.9137"/>
<vertex x="-0.635" y="-0.9137" curve="-90"/>
<vertex x="-1.5875" y="0" curve="-90"/>
</polygon>
<polygon width="0.254" layer="94">
<vertex x="0.635" y="-0.9137"/>
<vertex x="0.635" y="0.9137" curve="-90"/>
<vertex x="1.5875" y="0" curve="-90"/>
</polygon>
</symbol>
<symbol name="FUSE_PTC">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="1.7" y1="1.7" x2="2.5" y2="1.7" width="0.254" layer="94"/>
<wire x1="1.7" y1="1.7" x2="-1.7" y2="-1.7" width="0.254" layer="94"/>
<wire x1="-1.7" y1="-1.7" x2="-2.5" y2="-1.7" width="0.254" layer="94"/>
<text x="-3.81" y="2.0986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-4.002" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="2.54" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="2.54" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SUPPLY_GND" prefix="GND">
<gates>
<gate name="G$1" symbol="SUPPLY_GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SUPPLY_+3V3" prefix="+3V3">
<gates>
<gate name="G$1" symbol="SUPPLY_+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-0805" package="R805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="R805"/>
</technologies>
</device>
<device name="-0603" package="R603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0402" package="R402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-PTH-0.4" package="RTH025W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-TO-220-2" package="TO-220-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JUMPER_SOLDER_1X2" prefix="J">
<gates>
<gate name="J" symbol="JUMP_SOLDER_1X2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="JUMPER_SOLDER_1X2">
<connects>
<connect gate="J" pin="1" pad="1"/>
<connect gate="J" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FUSE_PTC" prefix="F" uservalue="yes">
<description>Resettable PTC</description>
<gates>
<gate name="G$1" symbol="FUSE_PTC" x="0" y="0"/>
</gates>
<devices>
<device name="-0805" package="PTC0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1812" package="PTC1812L">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED" prefix="LED" uservalue="yes">
<description>Light Emitting Diode</description>
<gates>
<gate name="LED" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="-3MM" package="LED-3MM">
<connects>
<connect gate="LED" pin="A" pad="A"/>
<connect gate="LED" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5MM" package="LED-5MM">
<connects>
<connect gate="LED" pin="A" pad="A"/>
<connect gate="LED" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0805" package="LED-805">
<connects>
<connect gate="LED" pin="A" pad="A"/>
<connect gate="LED" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5MM-90DEG" package="LED-5MM-90DEG">
<connects>
<connect gate="LED" pin="A" pad="A"/>
<connect gate="LED" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0603" package="LED-603">
<connects>
<connect gate="LED" pin="A" pad="A"/>
<connect gate="LED" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITOR_NPOL" prefix="C" uservalue="yes">
<description>Non-Polarized capacitor in various packages</description>
<gates>
<gate name="C" symbol="CAP" x="0" y="-2.54"/>
</gates>
<devices>
<device name="-0805" package="C805">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0402" package="C402">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0603" package="C603">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1206" package="C1206">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-PTH_0.1&quot;" package="C0.1PTH">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="A1" library="LPCModv1" deviceset="TIVA_BOOSTER_HEADERS_4X10" device=""/>
<part name="LPC" library="LPCModv1" deviceset="CON_LPC_MIN" device=""/>
<part name="U1" library="LPCModv1" deviceset="SST26VF032" device=""/>
<part name="GND4" library="dp_devices" deviceset="SUPPLY_GND" device=""/>
<part name="+3V34" library="dp_devices" deviceset="SUPPLY_+3V3" device=""/>
<part name="GND3" library="dp_devices" deviceset="SUPPLY_GND" device=""/>
<part name="+3V31" library="dp_devices" deviceset="SUPPLY_+3V3" device=""/>
<part name="GND1" library="dp_devices" deviceset="SUPPLY_GND" device=""/>
<part name="+3V33" library="dp_devices" deviceset="SUPPLY_+3V3" device=""/>
<part name="GND2" library="dp_devices" deviceset="SUPPLY_GND" device=""/>
<part name="+3V32" library="dp_devices" deviceset="SUPPLY_+3V3" device=""/>
<part name="R5" library="dp_devices" deviceset="RESISTOR" device="-0805" technology="R805" value="330"/>
<part name="R6" library="dp_devices" deviceset="RESISTOR" device="-0805" technology="R805" value="330"/>
<part name="R7" library="dp_devices" deviceset="RESISTOR" device="-0805" technology="R805" value="330"/>
<part name="R11" library="dp_devices" deviceset="RESISTOR" device="-0805" technology="R805" value="330"/>
<part name="R8" library="dp_devices" deviceset="RESISTOR" device="-0805" technology="R805" value="330"/>
<part name="R10" library="dp_devices" deviceset="RESISTOR" device="-0805" technology="R805" value="330"/>
<part name="R9" library="dp_devices" deviceset="RESISTOR" device="-0805" technology="R805" value="330"/>
<part name="D1" library="LPCModv1" deviceset="DO323-DIODE" device=""/>
<part name="D2" library="LPCModv1" deviceset="DO323-DIODE" device=""/>
<part name="D3" library="LPCModv1" deviceset="DO323-DIODE" device=""/>
<part name="D7" library="LPCModv1" deviceset="DO323-DIODE" device=""/>
<part name="D4" library="LPCModv1" deviceset="DO323-DIODE" device=""/>
<part name="D6" library="LPCModv1" deviceset="DO323-DIODE" device=""/>
<part name="D5" library="LPCModv1" deviceset="DO323-DIODE" device=""/>
<part name="GND7" library="dp_devices" deviceset="SUPPLY_GND" device=""/>
<part name="GND8" library="dp_devices" deviceset="SUPPLY_GND" device=""/>
<part name="GND9" library="dp_devices" deviceset="SUPPLY_GND" device=""/>
<part name="GND10" library="dp_devices" deviceset="SUPPLY_GND" device=""/>
<part name="GND11" library="dp_devices" deviceset="SUPPLY_GND" device=""/>
<part name="GND13" library="dp_devices" deviceset="SUPPLY_GND" device=""/>
<part name="GND12" library="dp_devices" deviceset="SUPPLY_GND" device=""/>
<part name="+3V36" library="dp_devices" deviceset="SUPPLY_+3V3" device=""/>
<part name="D0" library="dp_devices" deviceset="JUMPER_SOLDER_1X2" device=""/>
<part name="F1" library="dp_devices" deviceset="FUSE_PTC" device="-0805"/>
<part name="BUSY" library="dp_devices" deviceset="LED" device="-0805"/>
<part name="DETECT" library="dp_devices" deviceset="LED" device="-0805"/>
<part name="WRITE" library="dp_devices" deviceset="LED" device="-0805"/>
<part name="R1" library="dp_devices" deviceset="RESISTOR" device="-0805" technology="R805" value="330"/>
<part name="R2" library="dp_devices" deviceset="RESISTOR" device="-0805" technology="R805" value="330"/>
<part name="R3" library="dp_devices" deviceset="RESISTOR" device="-0805" technology="R805" value="330"/>
<part name="R4" library="dp_devices" deviceset="RESISTOR" device="-0805" technology="R805" value="330"/>
<part name="READ" library="dp_devices" deviceset="LED" device="-0805"/>
<part name="+3V35" library="dp_devices" deviceset="SUPPLY_+3V3" device=""/>
<part name="C1" library="dp_devices" deviceset="CAPACITOR_NPOL" device="-0805"/>
<part name="GND5" library="dp_devices" deviceset="SUPPLY_GND" device=""/>
<part name="C2" library="dp_devices" deviceset="CAPACITOR_NPOL" device="-0805"/>
<part name="GND6" library="dp_devices" deviceset="SUPPLY_GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="A1" gate="G$1" x="40.64" y="45.72" smashed="yes"/>
<instance part="LPC" gate="G$1" x="111.76" y="45.72" rot="R180"/>
<instance part="U1" gate="G$1" x="83.82" y="83.82"/>
<instance part="GND4" gate="G$1" x="88.9" y="30.48"/>
<instance part="+3V34" gate="G$1" x="134.62" y="55.88"/>
<instance part="GND3" gate="G$1" x="83.82" y="45.72"/>
<instance part="+3V31" gate="G$1" x="15.24" y="66.04"/>
<instance part="GND1" gate="G$1" x="45.72" y="20.32"/>
<instance part="+3V33" gate="G$1" x="101.6" y="96.52"/>
<instance part="GND2" gate="G$1" x="66.04" y="68.58"/>
<instance part="+3V32" gate="G$1" x="53.34" y="81.28"/>
<instance part="R5" gate="G$1" x="165.1" y="58.42"/>
<instance part="R6" gate="G$1" x="165.1" y="48.26"/>
<instance part="R7" gate="G$1" x="165.1" y="38.1"/>
<instance part="R11" gate="G$1" x="210.82" y="48.26"/>
<instance part="R8" gate="G$1" x="165.1" y="27.94"/>
<instance part="R10" gate="G$1" x="210.82" y="58.42"/>
<instance part="R9" gate="G$1" x="182.88" y="114.3" rot="R270"/>
<instance part="D1" gate="G$1" x="180.34" y="55.88" rot="R90"/>
<instance part="D2" gate="G$1" x="180.34" y="45.72" rot="R90"/>
<instance part="D3" gate="G$1" x="180.34" y="35.56" rot="R90"/>
<instance part="D7" gate="G$1" x="223.52" y="45.72" rot="R90"/>
<instance part="D4" gate="G$1" x="180.34" y="25.4" rot="R90"/>
<instance part="D6" gate="G$1" x="223.52" y="55.88" rot="R90"/>
<instance part="D5" gate="G$1" x="182.88" y="104.14" rot="R90"/>
<instance part="GND7" gate="G$1" x="180.34" y="50.8"/>
<instance part="GND8" gate="G$1" x="180.34" y="40.64"/>
<instance part="GND9" gate="G$1" x="180.34" y="30.48"/>
<instance part="GND10" gate="G$1" x="180.34" y="20.32"/>
<instance part="GND11" gate="G$1" x="182.88" y="99.06"/>
<instance part="GND13" gate="G$1" x="223.52" y="40.64"/>
<instance part="GND12" gate="G$1" x="223.52" y="50.8"/>
<instance part="+3V36" gate="G$1" x="182.88" y="119.38"/>
<instance part="D0" gate="J" x="175.26" y="106.68"/>
<instance part="F1" gate="G$1" x="15.24" y="60.96" rot="R90"/>
<instance part="BUSY" gate="LED" x="134.62" y="88.9"/>
<instance part="DETECT" gate="LED" x="144.78" y="88.9"/>
<instance part="WRITE" gate="LED" x="154.94" y="88.9"/>
<instance part="R1" gate="G$1" x="134.62" y="96.52" rot="R270"/>
<instance part="R2" gate="G$1" x="144.78" y="96.52" rot="R270"/>
<instance part="R3" gate="G$1" x="154.94" y="96.52" rot="R270"/>
<instance part="R4" gate="G$1" x="165.1" y="96.52" rot="R270"/>
<instance part="READ" gate="LED" x="165.1" y="88.9"/>
<instance part="+3V35" gate="G$1" x="149.86" y="104.14"/>
<instance part="C1" gate="C" x="106.68" y="91.44"/>
<instance part="GND5" gate="G$1" x="106.68" y="86.36"/>
<instance part="C2" gate="C" x="134.62" y="43.18"/>
<instance part="GND6" gate="G$1" x="134.62" y="38.1"/>
</instances>
<busses>
</busses>
<nets>
<net name="LPC-CLK" class="0">
<segment>
<pinref part="LPC" gate="G$1" pin="1"/>
<wire x1="114.3" y1="38.1" x2="119.38" y2="38.1" width="0.1524" layer="91"/>
<label x="116.84" y="38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<pinref part="D1" gate="G$1" pin="C"/>
<wire x1="170.18" y1="58.42" x2="180.34" y2="58.42" width="0.1524" layer="91"/>
<junction x="180.34" y="58.42"/>
<wire x1="180.34" y1="58.42" x2="187.96" y2="58.42" width="0.1524" layer="91"/>
<label x="185.42" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="LPC-RST" class="0">
<segment>
<pinref part="LPC" gate="G$1" pin="5"/>
<wire x1="114.3" y1="43.18" x2="119.38" y2="43.18" width="0.1524" layer="91"/>
<label x="116.84" y="43.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<pinref part="D2" gate="G$1" pin="C"/>
<wire x1="170.18" y1="48.26" x2="180.34" y2="48.26" width="0.1524" layer="91"/>
<junction x="180.34" y="48.26"/>
<wire x1="180.34" y1="48.26" x2="187.96" y2="48.26" width="0.1524" layer="91"/>
<label x="187.96" y="48.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="LPC-D0" class="0">
<segment>
<pinref part="LPC" gate="G$1" pin="4"/>
<wire x1="106.68" y1="40.64" x2="101.6" y2="40.64" width="0.1524" layer="91"/>
<label x="96.52" y="40.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R9" gate="G$1" pin="2"/>
<pinref part="D5" gate="G$1" pin="C"/>
<wire x1="182.88" y1="109.22" x2="182.88" y2="106.68" width="0.1524" layer="91"/>
<junction x="182.88" y="106.68"/>
<wire x1="182.88" y1="106.68" x2="190.5" y2="106.68" width="0.1524" layer="91"/>
<label x="187.96" y="106.68" size="1.778" layer="95"/>
<pinref part="D0" gate="J" pin="2"/>
<wire x1="180.34" y1="106.68" x2="182.88" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="5V" class="0">
<segment>
<pinref part="LPC" gate="G$1" pin="6"/>
<wire x1="106.68" y1="43.18" x2="101.6" y2="43.18" width="0.1524" layer="91"/>
<label x="99.06" y="43.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$21"/>
<wire x1="30.48" y1="53.34" x2="35.56" y2="53.34" width="0.1524" layer="91"/>
<label x="33.02" y="53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="LPC-LAD3" class="0">
<segment>
<pinref part="LPC" gate="G$1" pin="7"/>
<wire x1="114.3" y1="45.72" x2="119.38" y2="45.72" width="0.1524" layer="91"/>
<label x="116.84" y="45.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R11" gate="G$1" pin="2"/>
<pinref part="D7" gate="G$1" pin="C"/>
<wire x1="215.9" y1="48.26" x2="223.52" y2="48.26" width="0.1524" layer="91"/>
<junction x="223.52" y="48.26"/>
<wire x1="223.52" y1="48.26" x2="231.14" y2="48.26" width="0.1524" layer="91"/>
<label x="231.14" y="50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="LPC-LAD0" class="0">
<segment>
<pinref part="LPC" gate="G$1" pin="11"/>
<wire x1="114.3" y1="50.8" x2="119.38" y2="50.8" width="0.1524" layer="91"/>
<label x="116.84" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R7" gate="G$1" pin="2"/>
<pinref part="D3" gate="G$1" pin="C"/>
<wire x1="170.18" y1="38.1" x2="180.34" y2="38.1" width="0.1524" layer="91"/>
<junction x="180.34" y="38.1"/>
<wire x1="180.34" y1="38.1" x2="187.96" y2="38.1" width="0.1524" layer="91"/>
<label x="187.96" y="38.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="LPC-LAD2" class="0">
<segment>
<pinref part="LPC" gate="G$1" pin="8"/>
<wire x1="106.68" y1="45.72" x2="101.6" y2="45.72" width="0.1524" layer="91"/>
<label x="93.98" y="45.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<pinref part="D6" gate="G$1" pin="C"/>
<wire x1="215.9" y1="58.42" x2="223.52" y2="58.42" width="0.1524" layer="91"/>
<junction x="223.52" y="58.42"/>
<wire x1="223.52" y1="58.42" x2="231.14" y2="58.42" width="0.1524" layer="91"/>
<label x="228.6" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="LPC-LAD1" class="0">
<segment>
<pinref part="LPC" gate="G$1" pin="10"/>
<wire x1="106.68" y1="48.26" x2="101.6" y2="48.26" width="0.1524" layer="91"/>
<label x="93.98" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<pinref part="D4" gate="G$1" pin="C"/>
<wire x1="170.18" y1="27.94" x2="180.34" y2="27.94" width="0.1524" layer="91"/>
<junction x="180.34" y="27.94"/>
<wire x1="180.34" y1="27.94" x2="187.96" y2="27.94" width="0.1524" layer="91"/>
<label x="187.96" y="27.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="LPC" gate="G$1" pin="12"/>
<wire x1="106.68" y1="50.8" x2="88.9" y2="50.8" width="0.1524" layer="91"/>
<wire x1="88.9" y1="50.8" x2="88.9" y2="38.1" width="0.1524" layer="91"/>
<pinref part="LPC" gate="G$1" pin="2"/>
<wire x1="88.9" y1="38.1" x2="106.68" y2="38.1" width="0.1524" layer="91"/>
<pinref part="GND4" gate="G$1" pin="GND"/>
<wire x1="88.9" y1="38.1" x2="88.9" y2="33.02" width="0.1524" layer="91"/>
<junction x="88.9" y="38.1"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$20"/>
<wire x1="71.12" y1="53.34" x2="83.82" y2="53.34" width="0.1524" layer="91"/>
<wire x1="83.82" y1="53.34" x2="83.82" y2="48.26" width="0.1524" layer="91"/>
<pinref part="GND3" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$22"/>
<wire x1="30.48" y1="50.8" x2="45.72" y2="50.8" width="0.1524" layer="91"/>
<wire x1="45.72" y1="50.8" x2="45.72" y2="22.86" width="0.1524" layer="91"/>
<pinref part="GND1" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VSS"/>
<pinref part="GND2" gate="G$1" pin="GND"/>
<wire x1="68.58" y1="76.2" x2="66.04" y2="76.2" width="0.1524" layer="91"/>
<wire x1="66.04" y1="76.2" x2="66.04" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D1" gate="G$1" pin="A"/>
<pinref part="GND7" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="D2" gate="G$1" pin="A"/>
<pinref part="GND8" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="D3" gate="G$1" pin="A"/>
<pinref part="GND9" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="D4" gate="G$1" pin="A"/>
<pinref part="GND10" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="D5" gate="G$1" pin="A"/>
<pinref part="GND11" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="D7" gate="G$1" pin="A"/>
<pinref part="GND13" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="D6" gate="G$1" pin="A"/>
<pinref part="GND12" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="C1" gate="C" pin="2"/>
<pinref part="GND5" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="C2" gate="C" pin="2"/>
<pinref part="GND6" gate="G$1" pin="GND"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="LPC" gate="G$1" pin="9"/>
<wire x1="114.3" y1="48.26" x2="134.62" y2="48.26" width="0.1524" layer="91"/>
<wire x1="134.62" y1="48.26" x2="134.62" y2="55.88" width="0.1524" layer="91"/>
<pinref part="+3V34" gate="G$1" pin="+3V3"/>
<pinref part="C2" gate="C" pin="1"/>
<junction x="134.62" y="48.26"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VDD"/>
<pinref part="+3V33" gate="G$1" pin="+3V3"/>
<wire x1="99.06" y1="91.44" x2="101.6" y2="91.44" width="0.1524" layer="91"/>
<wire x1="101.6" y1="91.44" x2="101.6" y2="96.52" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="SIO3"/>
<wire x1="99.06" y1="86.36" x2="101.6" y2="86.36" width="0.1524" layer="91"/>
<wire x1="101.6" y1="86.36" x2="101.6" y2="91.44" width="0.1524" layer="91"/>
<junction x="101.6" y="91.44"/>
<pinref part="C1" gate="C" pin="1"/>
<wire x1="106.68" y1="96.52" x2="101.6" y2="96.52" width="0.1524" layer="91"/>
<junction x="101.6" y="96.52"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="SIO2"/>
<wire x1="68.58" y1="81.28" x2="53.34" y2="81.28" width="0.1524" layer="91"/>
<pinref part="+3V32" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<pinref part="+3V36" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="+3V31" gate="G$1" pin="+3V3"/>
<pinref part="F1" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="154.94" y1="101.6" x2="165.1" y2="101.6" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="1"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="134.62" y1="101.6" x2="144.78" y2="101.6" width="0.1524" layer="91"/>
<wire x1="154.94" y1="101.6" x2="149.86" y2="101.6" width="0.1524" layer="91"/>
<wire x1="149.86" y1="101.6" x2="144.78" y2="101.6" width="0.1524" layer="91"/>
<wire x1="144.78" y1="101.6" x2="149.86" y2="101.6" width="0.1524" layer="91"/>
<wire x1="149.86" y1="101.6" x2="149.86" y2="104.14" width="0.1524" layer="91"/>
<pinref part="+3V35" gate="G$1" pin="+3V3"/>
<junction x="144.78" y="101.6"/>
<junction x="154.94" y="101.6"/>
<junction x="149.86" y="101.6"/>
</segment>
</net>
<net name="SPI-RX" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="SO/SIO1"/>
<wire x1="68.58" y1="86.36" x2="60.96" y2="86.36" width="0.1524" layer="91"/>
<label x="60.96" y="86.36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$13"/>
<wire x1="71.12" y1="35.56" x2="76.2" y2="35.56" width="0.1524" layer="91"/>
<label x="73.66" y="35.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="SPI-CLK" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="SCK"/>
<wire x1="99.06" y1="81.28" x2="106.68" y2="81.28" width="0.1524" layer="91"/>
<label x="101.6" y="81.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$11"/>
<wire x1="71.12" y1="30.48" x2="76.2" y2="30.48" width="0.1524" layer="91"/>
<label x="73.66" y="30.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="SPI-FSS" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="CE#"/>
<wire x1="68.58" y1="91.44" x2="60.96" y2="91.44" width="0.1524" layer="91"/>
<label x="60.96" y="91.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$12"/>
<wire x1="71.12" y1="33.02" x2="76.2" y2="33.02" width="0.1524" layer="91"/>
<label x="73.66" y="33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="SPI-TX" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="SI/SIO0"/>
<wire x1="99.06" y1="76.2" x2="106.68" y2="76.2" width="0.1524" layer="91"/>
<label x="101.6" y="76.2" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$8"/>
<wire x1="15.24" y1="35.56" x2="20.32" y2="35.56" width="0.1524" layer="91"/>
<label x="2.54" y="35.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="TIVA_LPC-RST" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="160.02" y1="48.26" x2="154.94" y2="48.26" width="0.1524" layer="91"/>
<label x="144.78" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$27"/>
<wire x1="30.48" y1="38.1" x2="35.56" y2="38.1" width="0.1524" layer="91"/>
<label x="33.02" y="38.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="TIVA_LPC-LAD1" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="160.02" y1="27.94" x2="154.94" y2="27.94" width="0.1524" layer="91"/>
<label x="142.24" y="27.94" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$24"/>
<wire x1="30.48" y1="45.72" x2="35.56" y2="45.72" width="0.1524" layer="91"/>
<label x="33.02" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="TIVA_LPC-LAD2" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="205.74" y1="58.42" x2="200.66" y2="58.42" width="0.1524" layer="91"/>
<label x="195.58" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$25"/>
<wire x1="30.48" y1="43.18" x2="35.56" y2="43.18" width="0.1524" layer="91"/>
<label x="33.02" y="43.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="TIVA_LPC-LAD3" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="205.74" y1="48.26" x2="200.66" y2="48.26" width="0.1524" layer="91"/>
<label x="195.58" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$26"/>
<wire x1="30.48" y1="40.64" x2="35.56" y2="40.64" width="0.1524" layer="91"/>
<label x="33.02" y="40.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="TIVA_LPC-CLK" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="160.02" y1="58.42" x2="154.94" y2="58.42" width="0.1524" layer="91"/>
<label x="144.78" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$5"/>
<wire x1="15.24" y1="43.18" x2="20.32" y2="43.18" width="0.1524" layer="91"/>
<label x="17.78" y="43.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="TIVA_LPC-D0" class="0">
<segment>
<pinref part="D0" gate="J" pin="1"/>
<wire x1="170.18" y1="106.68" x2="160.02" y2="106.68" width="0.1524" layer="91"/>
<label x="157.48" y="106.68" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$19"/>
<wire x1="71.12" y1="50.8" x2="76.2" y2="50.8" width="0.1524" layer="91"/>
<label x="73.66" y="50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="TIVA_LPC-LAD0" class="0">
<segment>
<pinref part="A1" gate="G$1" pin="P$23"/>
<wire x1="30.48" y1="48.26" x2="35.56" y2="48.26" width="0.1524" layer="91"/>
<label x="33.02" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="160.02" y1="38.1" x2="154.94" y2="38.1" width="0.1524" layer="91"/>
<label x="142.24" y="38.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="A1" gate="G$1" pin="P$1"/>
<pinref part="F1" gate="G$1" pin="1"/>
<wire x1="15.24" y1="55.88" x2="15.24" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="BUSY" gate="LED" pin="A"/>
<pinref part="R1" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="DETECT" gate="LED" pin="A"/>
<pinref part="R2" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="WRITE" gate="LED" pin="A"/>
<pinref part="R3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<pinref part="READ" gate="LED" pin="A"/>
</segment>
</net>
<net name="BUSY-LED" class="0">
<segment>
<pinref part="BUSY" gate="LED" pin="C"/>
<wire x1="134.62" y1="83.82" x2="134.62" y2="78.74" width="0.1524" layer="91"/>
<label x="134.62" y="76.2" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$10"/>
<wire x1="15.24" y1="30.48" x2="20.32" y2="30.48" width="0.1524" layer="91"/>
<label x="17.78" y="30.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="XBLAST-LED" class="0">
<segment>
<pinref part="DETECT" gate="LED" pin="C"/>
<wire x1="144.78" y1="83.82" x2="144.78" y2="78.74" width="0.1524" layer="91"/>
<label x="144.78" y="76.2" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$29"/>
<wire x1="30.48" y1="33.02" x2="35.56" y2="33.02" width="0.1524" layer="91"/>
<label x="33.02" y="33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="WRITE-LED" class="0">
<segment>
<pinref part="WRITE" gate="LED" pin="C"/>
<wire x1="154.94" y1="83.82" x2="154.94" y2="78.74" width="0.1524" layer="91"/>
<label x="154.94" y="76.2" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$9"/>
<wire x1="15.24" y1="33.02" x2="20.32" y2="33.02" width="0.1524" layer="91"/>
<label x="17.78" y="33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="READ-LED" class="0">
<segment>
<pinref part="READ" gate="LED" pin="C"/>
<wire x1="165.1" y1="83.82" x2="165.1" y2="78.74" width="0.1524" layer="91"/>
<label x="165.1" y="76.2" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$28"/>
<wire x1="30.48" y1="35.56" x2="35.56" y2="35.56" width="0.1524" layer="91"/>
<label x="33.02" y="35.56" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
