//
// This file is part of the GNU ARM Eclipse Plug-ins project.
// Copyright (c) 2014 Liviu Ionescu
//

// ----------------------------------------------------------------------------

#include "cortexm/ExceptionHandlers.h"

// ----------------------------------------------------------------------------

void __attribute__((weak))
Default_Handler(void);

// Forward declaration of the specific IRQ handlers. These are aliased
// to the Default_Handler, which is a 'forever' loop. When the application
// defines a handler (with the same name), this will automatically take
// precedence over these weak definitions
//
// TODO: Rename this and add the actual routines here.

void __attribute__ ((weak, alias ("Default_Handler")))
DeviceInterrupt_Handler(void);

extern void IntTimer0AHandler(void);
extern void IntTimer0BHandler(void);
extern void IntTimer1AHandler(void);
extern void IntTimer1BHandler(void);

// ----------------------------------------------------------------------------

extern unsigned int _estack;

typedef void
(* const pHandler)(void);

// ----------------------------------------------------------------------------

// The vector table.
// This relies on the linker script to place at correct location in memory.

__attribute__ ((section(".isr_vector"),used))
pHandler __isr_vectors[] =
  { //
    (pHandler) &_estack,                          // The initial stack pointer
        Reset_Handler,                            // The reset handler

        NMI_Handler,                              // The NMI handler
        HardFault_Handler,                        // The hard fault handler

#if defined(__ARM_ARCH_7M__) || defined(__ARM_ARCH_7EM__)
        MemManage_Handler,                        // The MPU fault handler
        BusFault_Handler,// The bus fault handler
        UsageFault_Handler,// The usage fault handler
#else
        0, 0, 0,				  // Reserved
#endif
        0,                                        // Reserved
        0,                                        // Reserved
        0,                                        // Reserved
        0,                                        // Reserved
        SVC_Handler,                              // SVCall handler
#if defined(__ARM_ARCH_7M__) || defined(__ARM_ARCH_7EM__)
        DebugMon_Handler,                         // Debug monitor handler
#else
        0,					  // Reserved
#endif
        0,                                        // Reserved
        PendSV_Handler,                           // The PendSV handler
        SysTick_Handler,                          // The SysTick handler

        // ----------------------------------------------------------------------
        // DEVICE vectors
        DeviceInterrupt_Handler,                      // GPIO Port A
		DeviceInterrupt_Handler,                      // GPIO Port B
		DeviceInterrupt_Handler,                      // GPIO Port C
		DeviceInterrupt_Handler,                      // GPIO Port D
		DeviceInterrupt_Handler,                      // GPIO Port E
		DeviceInterrupt_Handler,                      // UART0 Rx and Tx
		DeviceInterrupt_Handler,                      // UART1 Rx and Tx
		DeviceInterrupt_Handler,                      // SSI0 Rx and Tx
		DeviceInterrupt_Handler,                      // I2C0 Master and Slave
		DeviceInterrupt_Handler,                      // PWM Fault
		DeviceInterrupt_Handler,                      // PWM Generator 0
		DeviceInterrupt_Handler,                      // PWM Generator 1
		DeviceInterrupt_Handler,                      // PWM Generator 2
		DeviceInterrupt_Handler,                      // Quadrature Encoder 0
		DeviceInterrupt_Handler,                      // ADC Sequence 0
		DeviceInterrupt_Handler,                      // ADC Sequence 1
		DeviceInterrupt_Handler,                      // ADC Sequence 2
		DeviceInterrupt_Handler,                      // ADC Sequence 3
		DeviceInterrupt_Handler,                      // Watchdog timer
		IntTimer0AHandler,                      	  // Timer 0 subtimer A
		IntTimer0BHandler,                      	  // Timer 0 subtimer B
		IntTimer1AHandler,                      	  // Timer 1 subtimer A
		IntTimer1BHandler,                      	  // Timer 1 subtimer B
		DeviceInterrupt_Handler,                      // Timer 2 subtimer A
		DeviceInterrupt_Handler,                      // Timer 2 subtimer B
		DeviceInterrupt_Handler,                      // Analog Comparator 0
		DeviceInterrupt_Handler,                      // Analog Comparator 1
		DeviceInterrupt_Handler,                      // Analog Comparator 2
		DeviceInterrupt_Handler,                      // System Control (PLL, OSC, BO)
		DeviceInterrupt_Handler,                      // FLASH Control
		DeviceInterrupt_Handler,                      // GPIO Port F
		DeviceInterrupt_Handler,                      // GPIO Port G
		DeviceInterrupt_Handler,                      // GPIO Port H
		DeviceInterrupt_Handler,                      // UART2 Rx and Tx
		DeviceInterrupt_Handler,                      // SSI1 Rx and Tx
		DeviceInterrupt_Handler,                      // Timer 3 subtimer A
		DeviceInterrupt_Handler,                      // Timer 3 subtimer B
		DeviceInterrupt_Handler,                      // I2C1 Master and Slave
		DeviceInterrupt_Handler,                      // Quadrature Encoder 1
		DeviceInterrupt_Handler,                      // CAN0
		DeviceInterrupt_Handler,                      // CAN1
		0,                                      // Reserved
		0,                                      // Reserved
		DeviceInterrupt_Handler,                      // Hibernate
		DeviceInterrupt_Handler,                      // USB0
		DeviceInterrupt_Handler,                      // PWM Generator 3
		DeviceInterrupt_Handler,                      // uDMA Software Transfer
		DeviceInterrupt_Handler,                      // uDMA Error
		DeviceInterrupt_Handler,                      // ADC1 Sequence 0
		DeviceInterrupt_Handler,                      // ADC1 Sequence 1
		DeviceInterrupt_Handler,                      // ADC1 Sequence 2
		DeviceInterrupt_Handler,                      // ADC1 Sequence 3
		0,                                      // Reserved
		0,                                      // Reserved
		DeviceInterrupt_Handler,                      // GPIO Port J
		DeviceInterrupt_Handler,                      // GPIO Port K
		DeviceInterrupt_Handler,                      // GPIO Port L
		DeviceInterrupt_Handler,                      // SSI2 Rx and Tx
		DeviceInterrupt_Handler,                      // SSI3 Rx and Tx
		DeviceInterrupt_Handler,                      // UART3 Rx and Tx
		DeviceInterrupt_Handler,                      // UART4 Rx and Tx
		DeviceInterrupt_Handler,                      // UART5 Rx and Tx
		DeviceInterrupt_Handler,                      // UART6 Rx and Tx
		DeviceInterrupt_Handler,                      // UART7 Rx and Tx
		0,                                      // Reserved
		0,                                      // Reserved
		0,                                      // Reserved
		0,                                      // Reserved
		DeviceInterrupt_Handler,                      // I2C2 Master and Slave
		DeviceInterrupt_Handler,                      // I2C3 Master and Slave
		DeviceInterrupt_Handler,                      // Timer 4 subtimer A
		DeviceInterrupt_Handler,                      // Timer 4 subtimer B
		0,                                      // Reserved
		0,                                      // Reserved
		0,                                      // Reserved
		0,                                      // Reserved
		0,                                      // Reserved
		0,                                      // Reserved
		0,                                      // Reserved
		0,                                      // Reserved
		0,                                      // Reserved
		0,                                      // Reserved
		0,                                      // Reserved
		0,                                      // Reserved
		0,                                      // Reserved
		0,                                      // Reserved
		0,                                      // Reserved
		0,                                      // Reserved
		0,                                      // Reserved
		0,                                      // Reserved
		0,                                      // Reserved
		0,                                      // Reserved
		DeviceInterrupt_Handler,                      // Timer 5 subtimer A
		DeviceInterrupt_Handler,                      // Timer 5 subtimer B
		DeviceInterrupt_Handler,                      // Wide Timer 0 subtimer A
		DeviceInterrupt_Handler,                      // Wide Timer 0 subtimer B
		DeviceInterrupt_Handler,                      // Wide Timer 1 subtimer A
		DeviceInterrupt_Handler,                      // Wide Timer 1 subtimer B
		DeviceInterrupt_Handler,                      // Wide Timer 2 subtimer A
		DeviceInterrupt_Handler,                      // Wide Timer 2 subtimer B
		DeviceInterrupt_Handler,                      // Wide Timer 3 subtimer A
		DeviceInterrupt_Handler,                      // Wide Timer 3 subtimer B
		DeviceInterrupt_Handler,                      // Wide Timer 4 subtimer A
		DeviceInterrupt_Handler,                      // Wide Timer 4 subtimer B
		DeviceInterrupt_Handler,                      // Wide Timer 5 subtimer A
		DeviceInterrupt_Handler,                      // Wide Timer 5 subtimer B
		DeviceInterrupt_Handler,                      // FPU
		0,                                      // Reserved
		0,                                      // Reserved
		DeviceInterrupt_Handler,                      // I2C4 Master and Slave
		DeviceInterrupt_Handler,                      // I2C5 Master and Slave
		DeviceInterrupt_Handler,                      // GPIO Port M
		DeviceInterrupt_Handler,                      // GPIO Port N
		DeviceInterrupt_Handler,                      // Quadrature Encoder 2
		0,                                      // Reserved
		0,                                      // Reserved
		DeviceInterrupt_Handler,                      // GPIO Port P (Summary or P0)
		DeviceInterrupt_Handler,                      // GPIO Port P1
		DeviceInterrupt_Handler,                      // GPIO Port P2
		DeviceInterrupt_Handler,                      // GPIO Port P3
		DeviceInterrupt_Handler,                      // GPIO Port P4
		DeviceInterrupt_Handler,                      // GPIO Port P5
		DeviceInterrupt_Handler,                      // GPIO Port P6
		DeviceInterrupt_Handler,                      // GPIO Port P7
		DeviceInterrupt_Handler,                      // GPIO Port Q (Summary or Q0)
		DeviceInterrupt_Handler,                      // GPIO Port Q1
		DeviceInterrupt_Handler,                      // GPIO Port Q2
		DeviceInterrupt_Handler,                      // GPIO Port Q3
		DeviceInterrupt_Handler,                      // GPIO Port Q4
		DeviceInterrupt_Handler,                      // GPIO Port Q5
		DeviceInterrupt_Handler,                      // GPIO Port Q6
		DeviceInterrupt_Handler,                      // GPIO Port Q7
		DeviceInterrupt_Handler,                      // GPIO Port R
		DeviceInterrupt_Handler,                      // GPIO Port S
		DeviceInterrupt_Handler,                      // PWM 1 Generator 0
		DeviceInterrupt_Handler,                      // PWM 1 Generator 1
		DeviceInterrupt_Handler,                      // PWM 1 Generator 2
		DeviceInterrupt_Handler,                      // PWM 1 Generator 3
		DeviceInterrupt_Handler                       // PWM 1 Fault
    };

// ----------------------------------------------------------------------------

// Processor ends up here if an unexpected interrupt occurs or a specific
// handler is not present in the application code.

void __attribute__ ((section(".after_vectors")))
Default_Handler(void)
{
  while (1)
    {
    }
}

// ----------------------------------------------------------------------------
