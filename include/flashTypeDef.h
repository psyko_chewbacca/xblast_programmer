/*
 * flashTypeDef.h
 *
 *  Created on: 2015-08-10
 *      Author: benjaminfd
 */

#ifndef EXT_FLASHTYPEDEF_H_
#define EXT_FLASHTYPEDEF_H_

typedef struct {
    unsigned char m_bManufacturerId;
    unsigned char m_bDeviceId;
    char m_szFlashDescription[32];
    unsigned int m_dwLengthInBytes;
} KNOWN_FLASH_TYPE;

#endif /* EXT_FLASHTYPEDEF_H_ */
