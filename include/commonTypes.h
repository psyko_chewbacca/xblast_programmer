/*
 * commonTypes.h
 *
 *  Created on: 2015-09-01
 *      Author: benjaminfd
 */

#ifndef SRC_COMMONTYPES_H_
#define SRC_COMMONTYPES_H_

typedef enum
{
	WriteModchipMode = 0u,
	ReadModchipMode
}systemModeState;

typedef enum
{
	setBlinkingBusyLed = 0u,
	detectModchip,
	ReadVerifyModchipFlash,
	EraseModchipFlash,
	WriteReadVerifyModchipFlash,
	stopBlinkingBusyLed
}accessModchipSequence;

#endif /* SRC_COMMONTYPES_H_ */
