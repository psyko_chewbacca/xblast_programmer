/*
 * timeHandler.h
 *
 *  Created on: 2015-08-03
 *      Author: benjaminfd
 */

#ifndef SRC_TIMEHANDLER_H_
#define SRC_TIMEHANDLER_H_

//Using Timer0 as general timer for user interactions

void timerSetup(void);

void startTimer0A(void);
void startTimer0B(void);
void startTimer1A(void);
void stopTimer1A(void);
void startTimer1B(void);
void stopTimer1B(void);

extern bool timer0Arefresh;
extern bool timer0Brefresh;

#endif /* SRC_TIMEHANDLER_H_ */
