/*
 * modchipHandler.h
 *
 *  Created on: 2015-09-01
 *      Author: benjaminfd
 */

#ifndef SRC_MODCHIPHANDLER_H_
#define SRC_MODCHIPHANDLER_H_

#include "commonTypes.h"
#include <stdbool.h>

accessModchipSequence modchipDetectRoutine(systemModeState globalModeSet);

unsigned int getDetectedModchipFlashSize();

unsigned char modchipReadRoutine(unsigned int startAddr, unsigned char *dataBuf, unsigned int dataBufLength);

accessModchipSequence modchipEraseRoutine(unsigned int startAddr, unsigned int length);

unsigned char modchipWriteRoutine(unsigned int startAddr, unsigned char *dataBuf, unsigned int dataBufLength);

unsigned char modchipVerifyRoutine(unsigned int startAddr, unsigned char *dataBuf, unsigned int dataBufLength);

bool modchipDetected;

#endif /* SRC_MODCHIPHANDLER_H_ */
