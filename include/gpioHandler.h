/*
 * gpioHandler.h
 *
 *  Created on: 2015-07-31
 *      Author: benjaminfd
 */

#ifndef SRC_GPIOHANDLER_H_
#define SRC_GPIOHANDLER_H_

#include <stdint.h>
#include <stdbool.h>
#include "driverlib/gpio.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"

//Onboard LED GPIO bit mappings.
#define LED_RED GPIO_PIN_1
#define LED_BLUE GPIO_PIN_2
#define LED_GREEN GPIO_PIN_3

//Onboard tactile switches GPIO bit mappings.
#define ONBOARD_SW1 GPIO_PIN_0
#define ONBOARD_SW2 GPIO_PIN_4

//LEDs on Booster board
#define XBLAST_LED GPIO_PIN_2
#define READ_LED GPIO_PIN_3
#define WRITE_LED GPIO_PIN_6
#define BUSY_LED GPIO_PIN_7

//Optional D0 input test pin
#define MODCHIP_D0 GPIO_PIN_4

//SPI2 pin map
#define SPI2_CLK GPIO_PIN_4
#define SPI2_CS GPIO_PIN_5
#define SPI2_MISO GPIO_PIN_6
#define SPI2_MOSI GPIO_PIN_7

//LPC port
#define LPC_LAD0 GPIO_PIN_0
#define LPC_LAD1 GPIO_PIN_1
#define LPC_LAD2 GPIO_PIN_2
#define LPC_LAD3 GPIO_PIN_3
#define LPC_RST GPIO_PIN_1
#define LPC_CLK GPIO_PIN_4

typedef struct
{
	unsigned int PeriphBase;
	unsigned int PortBase;
	unsigned char pin;
}gpioPinConfig;

extern const gpioPinConfig xblastLed;
extern const gpioPinConfig readLed;
extern const gpioPinConfig busyLed;
extern const gpioPinConfig writeLed;

extern const gpioPinConfig modchipD0;

extern const gpioPinConfig spi2Clk;
extern const gpioPinConfig spi2Cs;
extern const gpioPinConfig spi2Miso;
extern const gpioPinConfig spi2Mosi;

extern const gpioPinConfig lpcLAD0;
extern const gpioPinConfig lpcLAD1;
extern const gpioPinConfig lpcLAD2;
extern const gpioPinConfig lpcLAD3;

extern const gpioPinConfig lpcRST;
extern const gpioPinConfig lpcCLK;

void gpioSetup(void);
void updateOnboardRGBLEDs(void);

bool readSW1State(void);
bool readSW2State(void);

void toggleBusyLed(void);

void setGPIOPinDir(gpioPinConfig pin, bool isInput);
void setGPIOPortDir(gpioPinConfig pin, char isInputMask, bool isInput);

void setGPIOPinOutputState(gpioPinConfig pin, bool state);
void setGPIOPortOutputState(gpioPinConfig pin, unsigned char mask, unsigned char state);

bool getGPIOPinInputState(gpioPinConfig pin);
char getGPIOPortInputState(gpioPinConfig pin);

#endif /* SRC_GPIOHANDLER_H_ */
