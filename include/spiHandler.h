/*
 * spiHandler.h
 *
 *  Created on: 2015-09-02
 *      Author: benjaminfd
 */

#ifndef SRC_SPIHANDLER_H_
#define SRC_SPIHANDLER_H_


void spiSetup(void);

void initSPIFlash(void);

unsigned char writeSPIFlash(unsigned int addr, unsigned char *data, unsigned int bufLength);
unsigned char readSPIFlash(unsigned int addr, unsigned char *data, unsigned int bufLength);
unsigned char verifySPIFlash(unsigned int addr, unsigned char *data, unsigned int bufLength);

#endif /* SRC_SPIHANDLER_H_ */
