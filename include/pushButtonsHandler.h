/*
 * psuhButtonsHandler.h
 *
 *  Created on: 2015-08-03
 *      Author: benjaminfd
 */

#ifndef SRC_PUSHBUTTONSHANDLER_H_
#define SRC_PUSHBUTTONSHANDLER_H_

#include <stdbool.h>

typedef struct
{
	bool modeChangeButton;
	bool startModeButton;
} buttonPressedStruct;

buttonPressedStruct pollTactileSwitchStates(void);

#endif /* SRC_PUSHBUTTONSHANDLER_H_ */
