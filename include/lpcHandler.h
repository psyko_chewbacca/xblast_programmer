/*
 * lpcHandler.h
 *
 *  Created on: 2015-08-10
 *      Author: benjaminfd
 */

#ifndef SRC_LPCHANDLER_H_
#define SRC_LPCHANDLER_H_

#include <stdbool.h>
#include "flashTypeDef.h"

void initLPCBusGPIOs(void);

void putDeviceInRSTState(bool state);
void dummyClockCycles(unsigned char numberOfCycles);

char readLPCFlashID(KNOWN_FLASH_TYPE *flash);
char readXboxModchipSYSCONID(unsigned char *syscon);

unsigned char blockEraseAttempt(unsigned int startAddrOffset, unsigned int lengthInBytes);
unsigned char sectorEraseAttempt(unsigned int startAddrOffset, unsigned int lengthInBytes);
unsigned char chipEraseAttempt(void);

unsigned char readLPCByte(unsigned int addr);
unsigned char writeLPCByte(unsigned int addr, unsigned char data);

#endif /* SRC_LPCHANDLER_H_ */
