/*
 * lpc_io.h
 *
 *  Created on: 2015-08-10
 *      Author: benjaminfd
 */

#ifndef SRC_LPC_IO_H_
#define SRC_LPC_IO_H_

void LPCInit(void);

void lpcRSTHigh(void);
void lpcRSTLow(void);

void lpcToggleClk(void);

char lpcReadMemByte(unsigned int addr, unsigned char *data);
char lpcWriteMemByte(unsigned int addr, unsigned char data);

char lpcReadIOByte(unsigned short addr, unsigned char *data);
char lpcWriteIOByte(unsigned short addr, unsigned char data);

#endif /* SRC_LPC_IO_H_ */
