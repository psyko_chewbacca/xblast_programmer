/*
 * timeHandler.c
 *
 *  Created on: 2015-08-03
 *      Author: benjaminfd
 */
#include <stdint.h>
#include <stdbool.h>

#include "inc/hw_memmap.h"
#include "driverlib/timer.h"
#include "driverlib/systick.h"
#include "driverlib/rom.h"
#include "inc/hw_ints.h"
#include "gpioHandler.h"
#include "timeHandler.h"

bool timer0Arefresh;
bool timer0Brefresh;

void SysTickIntHandler(void)
{

}

//Every 10ms or so
void IntTimer0AHandler(void)
{
	ROM_TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
	ROM_SysCtlDelay(3);
	timer0Arefresh = true;
}

//every 210ms or so
void IntTimer0BHandler(void)
{
	ROM_TimerIntClear(TIMER0_BASE, TIMER_TIMB_TIMEOUT);
	ROM_SysCtlDelay(3);
	timer0Brefresh = true;
}

//Every 210ms or so
void IntTimer1AHandler(void)
{
	ROM_TimerIntClear(TIMER1_BASE, TIMER_TIMA_TIMEOUT);
	ROM_SysCtlDelay(3);
	//Bug with Timer1A usage. Makes Timer0B always interrupt...
}

//every 210ms or so
void IntTimer1BHandler(void)
{
	ROM_TimerIntClear(TIMER1_BASE, TIMER_TIMB_TIMEOUT);
	ROM_SysCtlDelay(3);
	static unsigned char delayer = 0;
	if(delayer == 2)
	{
		delayer = 0;
		//Occurs around every 630ms
		toggleBusyLed();
	}
	else
	{
		delayer++;
	}
}

void timerSetup(void)
{
    //
    // Initialize the SysTick interrupt to process colors and buttons.
    //
    SysTickPeriodSet(SysCtlClockGet() / 32);
    SysTickEnable();
    SysTickIntEnable();

	//Set up 2 16-bit wide periodic timers.
	//Function below will turn off timer if needed.
	/*ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
	ROM_SysCtlDelay(3);
	ROM_TimerConfigure(TIMER0_BASE, TIMER_CFG_SPLIT_PAIR | TIMER_CFG_A_PERIODIC | TIMER_CFG_B_PERIODIC);

	//Set timers with max value in both. These are decrementing counters btw.
	ROM_TimerLoadSet(TIMER0_BASE, TIMER_BOTH, 0xFFFF);
	// Configure the counters to count the positive edges.
	ROM_TimerControlEvent(TIMER0_BASE, TIMER_BOTH, TIMER_EVENT_POS_EDGE);
	//Timer A will fire around every 10ms
	ROM_TimerPrescaleSet(TIMER0_BASE,TIMER_A, 13);
	//Timer B will fire around every 210ms.
	ROM_TimerPrescaleSet(TIMER0_BASE, TIMER_B, 255);

	ROM_IntEnable(INT_TIMER0A);
	ROM_IntEnable(INT_TIMER0B);
	ROM_TimerIntEnable(TIMER0_BASE, TIMER_TIMB_TIMEOUT| TIMER_TIMA_TIMEOUT);*/


	//Set up 2 16-bit wide periodic timers.
	//Function below will turn off timer if needed.
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1);
	ROM_SysCtlDelay(3);
	ROM_TimerConfigure(TIMER1_BASE, TIMER_CFG_SPLIT_PAIR | TIMER_CFG_A_PERIODIC | TIMER_CFG_B_PERIODIC);

	//Set timers with max value in both. These are decrementing counters btw.
	ROM_TimerLoadSet(TIMER1_BASE, TIMER_BOTH, 0xFFFF);
	// Configure the counters to count the positive edges.
	ROM_TimerControlEvent(TIMER1_BASE, TIMER_BOTH, TIMER_EVENT_POS_EDGE);
	//Timer A will fire around every 210ms
	ROM_TimerPrescaleSet(TIMER1_BASE,TIMER_A, 255);
	//Timer B will fire around every 210ms.
	ROM_TimerPrescaleSet(TIMER1_BASE, TIMER_B, 255);

	ROM_IntEnable(INT_TIMER1B);
	ROM_TimerIntEnable(TIMER1_BASE, TIMER_TIMB_TIMEOUT);

	/*ROM_TimerEnable(TIMER0_BASE, TIMER_BOTH);
	//Timer1 enabling will be done on demand.

    timer0Arefresh = false;
    timer0Brefresh = false;*/
}

void startTimer0A(void)
{
	ROM_TimerEnable(TIMER0_BASE, TIMER_A);
}

void startTimer0B(void)
{
	ROM_TimerEnable(TIMER0_BASE, TIMER_B);
}

void startTimer1A(void)
{
	//ROM_TimerEnable(TIMER1_BASE, TIMER_A);
}

void stopTimer1A(void)
{
	ROM_TimerDisable(TIMER1_BASE, TIMER_A);
}

void startTimer1B(void)
{
	ROM_TimerEnable(TIMER1_BASE, TIMER_B);
}

void stopTimer1B(void)
{
	ROM_TimerDisable(TIMER1_BASE, TIMER_B);
}
