/*
 * pushButtonsHandler.c
 *
 *  Created on: 2015-08-03
 *      Author: benjaminfd
 */

#include "pushButtonsHandler.h"
#include "gpioHandler.h"

#define pushButtonsDebounceThreshold	20

buttonPressedStruct pollTactileSwitchStates(void)
{
	static unsigned int SW1ConsecutivePositiveReads = 0;
	static unsigned int SW2ConsecutivePositiveReads = 0;
	buttonPressedStruct returnStruct;

	SW1ConsecutivePositiveReads = !readSW1State() ? SW1ConsecutivePositiveReads + 1 : 0;
	SW2ConsecutivePositiveReads = !readSW2State() ? SW2ConsecutivePositiveReads + 1 : 0;

	if(SW1ConsecutivePositiveReads == pushButtonsDebounceThreshold)
	{
		returnStruct.modeChangeButton = true;
	}
	else
	{
		returnStruct.modeChangeButton = false;
	}

	if(SW2ConsecutivePositiveReads == pushButtonsDebounceThreshold)
	{
		returnStruct.startModeButton = true;
	}
	else
	{
		returnStruct.startModeButton = false;
	}


	return returnStruct;
}
