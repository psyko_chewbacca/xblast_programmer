//
// This file is part of the GNU ARM Eclipse Plug-ins project.
// Copyright (c) 2014 Liviu Ionescu.
//

// ----------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "diag/Trace.h"
//#include "Timer.h"

#include "driverlib/rom_map.h"
#include "driverlib/interrupt.h"
#include "driverlib/sysctl.h"

#include "commonTypes.h"
#include "gpioHandler.h"
#include "timeHandler.h"
#include "spiHandler.h"
#include "pushButtonsHandler.h"
#include "lpcHandler.h"
#include "modchipHandler.h"
#include "flashTypeDef.h"
#include "BootLCD.h"
// ----------------------------------------------------------------------------
//
// Print a greeting message on the trace device and enter a loop
// to count seconds.
//
// Trace support is enabled by adding the TRACE macro definition.
// By default the trace messages are forwarded to the DEBUG output,
// but can be rerouted to any device or completely suppressed, by
// changing the definitions required in system/src/diag/trace_impl.c
// (currently OS_USE_TRACE_ITM, OS_USE_TRACE_SEMIHOSTING_DEBUG/_STDOUT).
//
// ----------------------------------------------------------------------------

// Sample pragmas to cope with warnings. Please note the related line at
// the end of this function, used to pop the compiler diagnostics status.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wmissing-declarations"
#pragma GCC diagnostic ignored "-Wreturn-type"

int
main (int argc, char* argv[])
{
  // Normally at this stage most of the microcontroller subsystems, including
  // the clock, were initialised by the CMSIS SystemInit() function invoked
  // from the startup file, before calling main().
  // (see system/src/cortexm/_initialize_hardware.c)
  // If further initialisations are required, customise __initialize_hardware()
  // or add the additional initialisation here, for example:
  //
  // HAL_Init();

  // In this sample the SystemInit() function is just a placeholder,
  // if you do not add the real one, the clock will remain configured with
  // the reset value, usually a relatively low speed RC clock (8-12MHz).

  IntMasterDisable();
  //80MHz clock set
  MAP_SysCtlClockSet(SYSCTL_SYSDIV_2_5|SYSCTL_USE_PLL|SYSCTL_OSC_MAIN|SYSCTL_XTAL_16MHZ);

	buttonPressedStruct pressedButtons;
	unsigned char globalModeSet = WriteModchipMode;

	bool goAction = false;
	unsigned char goActionCurrentTask = setBlinkingBusyLed;

    timerSetup();
    gpioSetup();
    //spiSetup();
    BootLCDInit();	//In case we need it.

    IntMasterEnable();

  // Send a greeting to the trace device (skipped on Release).
  trace_puts("Hello ARM World!");

  // At this stage the system clock should have already been configured
  // at high speed.
  trace_printf("System clock: %u MHz\n", MAP_SysCtlClockGet() / 1000000);

  const unsigned short sizeOfSPI_LPC_Buffer = 4 * 1024 * sizeof(unsigned char);
  unsigned char SPI_LPC_TransferBuf_4KB[sizeOfSPI_LPC_Buffer];	//4KB buf pre-allocated.

  unsigned int flashChipReadTotalSize;
  initLPCBusGPIOs();


  // Infinite loop
  while (1)
    {
	  //Occurs around every 10ms
	  updateLCD();

	   if(1)
	   {
		   //Read onboard tactile switches state.
		   pressedButtons = pollTactileSwitchStates();

		   if(pressedButtons.modeChangeButton)
		   {
			   switch(globalModeSet)
			   {
			   case WriteModchipMode:
				   globalModeSet = ReadModchipMode;

				   break;
			   case ReadModchipMode:
				   globalModeSet = WriteModchipMode;
				   break;
			   }
		   }

		   //LEDs are wired inverted. GPIO act as a sink source.
		   switch(globalModeSet)
		   {
		   case WriteModchipMode:
			 setGPIOPinOutputState(writeLed, false);
			 setGPIOPinOutputState(readLed, true);
			   break;
		   case ReadModchipMode:
			 setGPIOPinOutputState(writeLed, true);
			 setGPIOPinOutputState(readLed, false);
			   break;
		   }

		   if(pressedButtons.startModeButton)
		   {
			   goAction = true;
		   }

		   timer0Arefresh = false;
		   //startTimer0A();
	   }

	   //Occurs around every 210ms
	   if(timer0Brefresh)
	   {
		   //Refresh onboard RGB LEDs
		   updateOnboardRGBLEDs();
		   timer0Brefresh = false;
		   //startTimer0B();
	   }

	   if(goAction == true)
	   {

		   //goAction = false;
		   if(goActionCurrentTask == setBlinkingBusyLed)
		   {
			   //startTimer1B();
			 goActionCurrentTask = detectModchip;
		   }
		   else if(goActionCurrentTask == detectModchip)
		   {
			   //Will detect modchip and output next action according to the findings.
			   goActionCurrentTask = modchipDetectRoutine(globalModeSet);
		   }
		   else
		   {
			   switch(globalModeSet)
			   {
			   case WriteModchipMode:
				   switch(goActionCurrentTask)
				   {
				   case EraseModchipFlash:
					   //Try block erase(64KB) and then sector erase(4KB)
					   goActionCurrentTask = modchipEraseRoutine(0x0, getDetectedModchipFlashSize());
					   break;
				   case WriteReadVerifyModchipFlash:
					   //1- SPI read chunk(4KB?)
					   //2- call routine that writes, read back and verify against buffer pulled from SPI.
					   break;
				   default:
					   break;
				   }
				   break;
			   case ReadModchipMode:
				   flashChipReadTotalSize = getDetectedModchipFlashSize();

				   //goActionCurrentTask is technically set to ReadVerifyModchipFlash but not necessary to assert.
				   for(unsigned int readPositionMarker = 0; readPositionMarker < flashChipReadTotalSize; readPositionMarker += sizeOfSPI_LPC_Buffer)
				   {
					   //1-Read chunk from LPC modchip(4KB?)
					   if(modchipReadRoutine(readPositionMarker, SPI_LPC_TransferBuf_4KB, sizeOfSPI_LPC_Buffer))
					   {
						   goActionCurrentTask = stopBlinkingBusyLed;
						   break;
					   }

					   //2-Write chunk to SPI flash
					   if(writeSPIFlash(readPositionMarker, SPI_LPC_TransferBuf_4KB, sizeOfSPI_LPC_Buffer))
					   {
						   goActionCurrentTask = stopBlinkingBusyLed;
						   break;
					   }
				   }
				   //3-Read back chuck from SPI flash and verify against buffer
				   break;
			   }
		   }

		   if(goActionCurrentTask == stopBlinkingBusyLed)
		   {
			   //stopTimer1B();
			   goActionCurrentTask = setBlinkingBusyLed;
			   goAction = false;
			   //setGPIOPinOutputState(xblastLed, false);
			   modchipDetected = false;
		   }
	   }

    }
  // Infinite loop, never return.
}

#pragma GCC diagnostic pop

// ----------------------------------------------------------------------------
