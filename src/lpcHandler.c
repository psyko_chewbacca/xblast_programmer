/*
 * lpcHandler.c
 *
 *  Created on: 2015-08-10
 *      Author: benjaminfd
 */

#include "lpcHandler.h"
#include "lpc_io.h"

#include <string.h>

#define ERASE_SUBCMD_HEX_COMMAND 0x80
#define BLOCK_ERASE_HEX_COMMAND	0x50
#define SECTOR_ERASE_HEX_COMMAND 0x30
#define CHIP_ERASE_HEX_COMMAND 0x10
#define SOFT_ID_ENTRY_HEX_COMMAND	0x90
#define BYTE_PROGRAM_HEX_COMMAND	0xA0

void initLPCBusGPIOs(void)
{
	LPCInit();
}

void putDeviceInRSTState(bool state)
{
	if(state)
		lpcRSTLow();
	else
		lpcRSTHigh();
}

void dummyClockCycles(unsigned char numberOfCycles)
{
	unsigned char i;
	for(i = 0; i < numberOfCycles; i++)
		lpcToggleClk();
}

char readLPCFlashID(KNOWN_FLASH_TYPE *flash)
{
	// A bit hacky, but easier to maintain.
	const KNOWN_FLASH_TYPE aknownflashtypesDefault[] = {
		#include "flashtypes.h"
	};
	unsigned int i = 0;
	unsigned char manID, devID;
	// make sure the flash state machine is reset
	lpcWriteMemByte(0xFFFF5555, 0xf0);
	lpcWriteMemByte(0xFFFF5555, 0xaa);
	lpcWriteMemByte(0xFFFF2aaa, 0x55);
	lpcWriteMemByte(0xFFFF5555, 0xf0);

	// read flash ID

	lpcWriteMemByte(0xFFFF5555, 0xaa);
	lpcWriteMemByte(0xFFFF2aaa, 0x55);
	lpcWriteMemByte(0xFFFF5555, SOFT_ID_ENTRY_HEX_COMMAND);

	lpcReadMemByte(0xFFFF0000, &manID);
	lpcReadMemByte(0xFFFF0001, &devID);

	//Exit software ID
	lpcWriteMemByte(0xFFFF5555, 0xf0);

	while(aknownflashtypesDefault[i].m_bManufacturerId != 0)
	{
		if(aknownflashtypesDefault[i].m_bManufacturerId == manID &&
			aknownflashtypesDefault[i].m_bDeviceId == devID)
		{
			memcpy(flash, aknownflashtypesDefault + i, sizeof(KNOWN_FLASH_TYPE));
			return 0;
		}
		else
		{
			i++;
		}
	}

	return -1;
}

char readXboxModchipSYSCONID(unsigned char *syscon)
{
	return lpcReadIOByte(0xF701, syscon);
}

unsigned char blockEraseAttempt(unsigned int startAddrOffset, unsigned int lengthInBytes)
{
#define FLASH_BLOCK_SIZE 64*1024
	//Start with sanity checks
	//If start address offset is not a multiple or 64KB or is farther than start of a third bank on a 1MB memory space.
	if(startAddrOffset % (FLASH_BLOCK_SIZE) || startAddrOffset > (3 * 256 * 1024))
	{
		return 1;	//Invalid start offset
	}

	//If length in bytes to erase is not a multiple of 64KB or bigger than 1MB.
	if(lengthInBytes % (FLASH_BLOCK_SIZE) >> lengthInBytes > (1024 * 1024))
	{
		return 2;
	}

	startAddrOffset |= 0xFFF00000;	//Xbox sets 12 upper bits to '1', we do it too.

	for(unsigned int eraseOffset = startAddrOffset; eraseOffset < (startAddrOffset + lengthInBytes); eraseOffset += FLASH_BLOCK_SIZE)
	{

		lpcWriteMemByte(0xFFFF5555, 0xaa);
		lpcWriteMemByte(0xFFFF2aaa, 0x55);
		lpcWriteMemByte(0xFFFF5555, ERASE_SUBCMD_HEX_COMMAND);
		lpcWriteMemByte(0xFFFF5555, 0xaa);
		lpcWriteMemByte(0xFFFF2aaa, 0x55);
		lpcWriteMemByte(eraseOffset, BLOCK_ERASE_HEX_COMMAND);

		unsigned char busyBitRead1;
		lpcReadMemByte(eraseOffset, &busyBitRead1);
		unsigned char busyBitRead2 = ~busyBitRead1;
		unsigned int toggleCount = 0;

        while((busyBitRead1 & 40) != (busyBitRead2 & 0x40)) {
        	toggleCount++;
        	lpcReadMemByte(eraseOffset, &busyBitRead2);
        	busyBitRead1^=0x40;
        }

        if(toggleCount < 3)
        {
        	return 3;	//Block Erase is not supported.
        }
	}

	return 0;
}

unsigned char sectorEraseAttempt(unsigned int startAddrOffset, unsigned int lengthInBytes)
{
#define FLASH_SECTOR_SIZE 4*1024
	//Start with sanity checks
	//If start address offset is FLASH_SECTOR_SIZE a multiple or 64KB or is farther than start of a third bank on a 1MB memory space.
	if(startAddrOffset % (FLASH_BLOCK_SIZE) || startAddrOffset > (3 * 256 * 1024))
	{
		return 1;	//Invalid start offset
	}

	//If length in bytes to erase is not a multiple of 64KB or bigger than 1MB.
	if(lengthInBytes % (FLASH_SECTOR_SIZE) >> lengthInBytes > (1024 * 1024))
	{
		return 2;
	}

	startAddrOffset |= 0xFFF00000;	//Xbox sets 12 upper bits to '1', we do it too.

	for(unsigned int eraseOffset = startAddrOffset; eraseOffset < (startAddrOffset + lengthInBytes); eraseOffset += FLASH_SECTOR_SIZE)
	{

		lpcWriteMemByte(0xFFFF5555, 0xaa);
		lpcWriteMemByte(0xFFFF2aaa, 0x55);
		lpcWriteMemByte(0xFFFF5555, ERASE_SUBCMD_HEX_COMMAND);
		lpcWriteMemByte(0xFFFF5555, 0xaa);
		lpcWriteMemByte(0xFFFF2aaa, 0x55);
		lpcWriteMemByte(eraseOffset, SECTOR_ERASE_HEX_COMMAND);

		unsigned char busyBitRead1;
		lpcReadMemByte(eraseOffset, &busyBitRead1);
		unsigned char busyBitRead2 = ~busyBitRead1;
		unsigned int toggleCount = 0;

        while((busyBitRead1 & 40) != (busyBitRead2 & 0x40)) {
        	toggleCount++;
        	lpcReadMemByte(eraseOffset, &busyBitRead2);
        	busyBitRead1^=0x40;
        }

        if(toggleCount < 3)
        {
        	return 3;	//sector Erase is not supported.
        }
	}

	return 0;
}

unsigned char chipEraseAttempt(void)
{
	lpcWriteMemByte(0xFFFF5555, 0xaa);
	lpcWriteMemByte(0xFFFF2aaa, 0x55);
	lpcWriteMemByte(0xFFFF5555, ERASE_SUBCMD_HEX_COMMAND);
	lpcWriteMemByte(0xFFFF5555, 0xaa);
	lpcWriteMemByte(0xFFFF2aaa, 0x55);
	lpcWriteMemByte(0xFFFF5555, CHIP_ERASE_HEX_COMMAND);

	unsigned char busyBitRead1;
	lpcReadMemByte(0xFFFF5555, &busyBitRead1);
	unsigned char busyBitRead2 = ~busyBitRead1;
	unsigned int toggleCount = 0;

	while((busyBitRead1 & 40) != (busyBitRead2 & 0x40)) {
		toggleCount++;
		lpcReadMemByte(0xFFFF5555, &busyBitRead2);
		busyBitRead1^=0x40;
	}

	if(toggleCount < 3)
	{
		return 3;	//chip Erase is not supported.
	}

	return 0;
}

unsigned char readLPCByte(unsigned int addr)
{
	unsigned char byte;
	while(lpcReadMemByte(addr, &byte));	//will block.

	return byte;
}

unsigned char writeLPCByte(unsigned int addr, unsigned char data)
{
	return lpcWriteMemByte(addr, data);
}
