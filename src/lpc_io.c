/*
 * lpc_io.c
 *
 *  Created on: 2015-08-10
 *      Author: benjaminfd
 */

#include "gpioHandler.h"
#include "inc/hw_gpio.h"
#include "inc/hw_types.h"
#include <stddef.h>


void lpcRSTHigh(void)
{
	HWREG(lpcRST.PortBase + (GPIO_O_DATA + (lpcRST.pin << 2))) = lpcRST.pin;
}

void lpcRSTLow(void)
{
	HWREG(lpcRST.PortBase + (GPIO_O_DATA + (lpcRST.pin << 2))) = 0;
}


static inline void lpcCLKHigh(void)
{
	HWREG(lpcCLK.PortBase + (GPIO_O_DATA + (lpcCLK.pin << 2))) = lpcCLK.pin;
}

static inline void lpcCLKLow(void)
{
	HWREG(lpcCLK.PortBase + (GPIO_O_DATA + (lpcCLK.pin << 2))) = 0;
}

static inline void lpcLADInput(void)
{
	//setGPIOPortDir(lpcLAD0, 0xF, true);	//For speed
	HWREG(lpcLAD0.PortBase + GPIO_O_DIR) = ((GPIO_DIR_MODE_IN & 1) ?
	                                    (HWREG(lpcLAD0.PortBase + GPIO_O_DIR) | 0xF) :
	                                    (HWREG(lpcLAD0.PortBase + GPIO_O_DIR) & ~(0xF)));

	HWREG(lpcLAD0.PortBase + GPIO_O_AFSEL) = ((GPIO_DIR_MODE_IN & 2) ?
									  (HWREG(lpcLAD0.PortBase + GPIO_O_AFSEL) |
											  0xF) :
									  (HWREG(lpcLAD0.PortBase + GPIO_O_AFSEL) &
									   ~(0xF)));
}

static inline void lpcLADOutput(void)
{
	//setGPIOPortDir(lpcLAD0, 0xF, false);
	HWREG(lpcLAD0.PortBase + GPIO_O_DIR) = ((GPIO_DIR_MODE_OUT & 1) ?
	                                    (HWREG(lpcLAD0.PortBase + GPIO_O_DIR) | 0xF) :
	                                    (HWREG(lpcLAD0.PortBase + GPIO_O_DIR) & ~(0xF)));

	HWREG(lpcLAD0.PortBase + GPIO_O_AFSEL) = ((GPIO_DIR_MODE_OUT & 2) ?
									  (HWREG(lpcLAD0.PortBase + GPIO_O_AFSEL) |
											  0xF) :
									  (HWREG(lpcLAD0.PortBase + GPIO_O_AFSEL) &
									   ~(0xF)));
}

static inline char lpcNibbleRead(void)
{
	/*
	 * Writing then reading to a GPIO port does not guarantee that the input
	 * data is latched after the output has been driven. This effect can be
	 * observed regardless of the clock frequency. A memory barrier does not
	 * solve the issue; however a delay of at least four "nop" ensures that
	 * the data is latched after the output has been written. The four "nop"
	 * delay is independent of the core clock.
	 */
	__asm("nop");
	__asm("nop");
	__asm("nop");
	__asm("nop");
	//Doubled for debug
	__asm("nop");
	__asm("nop");
	__asm("nop");
	__asm("nop");
	return HWREG(lpcLAD0.PortBase + (GPIO_O_DATA + (0xF << 2)));

}

static inline void lpcNibbleWrite(char shiftedNibble)
{
	HWREG(lpcLAD0.PortBase + (GPIO_O_DATA + (0xF << 2))) = shiftedNibble;
}
static inline void startSequenceWithCYCType(unsigned char cycType)
{
	lpcLADOutput();
	lpcCLKLow();

	/* 1: START */
	lpcNibbleWrite(0);
	lpcCLKHigh();
	lpcCLKLow();

	/* 2: Cycle Type and direction */
	//cycType can be:
	//0x4 for MemRead ops
	//0x6 for MemWrite ops
	//0x0 for IORead ops
	//0x2 for IOWrite ops
	lpcNibbleWrite(cycType);
	lpcCLKHigh();
	lpcCLKLow();
}

static inline void sendMemAddress(uint32_t addr)
{
	/* Keep this out of a loop. We get a 40% throughput increase. */
	lpcNibbleWrite(addr >> 28);
	lpcCLKHigh();
	lpcCLKLow();
	lpcNibbleWrite(addr >> 24);
	lpcCLKHigh();
	lpcCLKLow();
	lpcNibbleWrite(addr >> 20);
	lpcCLKHigh();
	lpcCLKLow();
	lpcNibbleWrite(addr >> 16);
	lpcCLKHigh();
	lpcCLKLow();
	lpcNibbleWrite(addr >> 12);
	lpcCLKHigh();
	lpcCLKLow();
	lpcNibbleWrite(addr >> 8);
	lpcCLKHigh();
	lpcCLKLow();
	lpcNibbleWrite(addr >> 4);
	lpcCLKHigh();
	lpcCLKLow();
	lpcNibbleWrite(addr);
	lpcCLKHigh();
	lpcCLKLow();
}

static inline void sendIoAddress(uint16_t addr)
{
	/* Keep this out of a loop. We get a 40% throughput increase. */
	lpcNibbleWrite(addr >> 12);
	lpcCLKHigh();
	lpcCLKLow();
	lpcNibbleWrite(addr >> 8);
	lpcCLKHigh();
	lpcCLKLow();
	lpcNibbleWrite(addr >> 4);
	lpcCLKHigh();
	lpcCLKLow();
	lpcNibbleWrite(addr);
	lpcCLKHigh();
	lpcCLKLow();
}

static inline char tarWriteToRead(void)
{
	unsigned char result;

	lpcNibbleWrite(0xf);
	//TAR0
	lpcLADInput();
	lpcCLKHigh();
	lpcCLKLow();
	//TAR1
	lpcCLKHigh();
	result = lpcNibbleRead();
	lpcCLKLow();

	return result;
}

static inline void tarReadToWrite(void)
{
	//TAR2
	lpcCLKHigh();
	//I really don't care if slave device doesn't output 0xf here. Maybe I should?
	lpcCLKLow();
	//TAR3
	lpcLADOutput();
	lpcNibbleWrite(0xf);
	lpcCLKHigh();
	lpcCLKLow();
}

void lpcToggleClk(void)
{
	lpcCLKHigh();
	lpcCLKLow();
}


//Below are accessible from outside.
//Make sure lpcRST signal has been raised prior doing any LPC IOs.
char lpcReadMemByte(unsigned int addr, unsigned char *data)
{
	char syncRead;

	if(data == NULL)
		return -2;

	//LPC cycles 1,2
	startSequenceWithCYCType(0x4);
	//LPC cycles 3-10
	sendMemAddress(addr);

	//LPC cycles 11-12
	syncRead = tarWriteToRead();

	if (!syncRead)
			goto skip_sync_at_lpc_read;

	//LPC cycles 13
	lpcCLKHigh();
	if ((syncRead = lpcNibbleRead()) != 0)
	{
		*data = 0xff;
		return -1;
	}

	lpcCLKLow();

 skip_sync_at_lpc_read:

	//LPC cycle 14: getting data's lower nibble
	lpcCLKHigh();
	*data = lpcNibbleRead();
	lpcCLKLow();

	//LPC cycle 15: getting data's high nibble
	lpcCLKHigh();
	*data |= (lpcNibbleRead() << 4);
	lpcCLKLow();

	//LPC cycles 16,17
	tarReadToWrite();

	//Some dummy CLK cycles.
	lpcCLKHigh();
	lpcCLKLow();

	lpcCLKHigh();
	lpcCLKLow();

	return 0;
}

char lpcWriteMemByte(unsigned int addr, unsigned char data)
{
	unsigned char syncRead;

	//LPC cycles 1,2
	startSequenceWithCYCType(0x6);
	//LPC cycles 3-10
	sendMemAddress(addr);

	//LPC cycle 11
	lpcNibbleWrite(data);
	lpcCLKHigh();
	lpcCLKLow();

	//LPC cycle 12
	lpcNibbleWrite((data >> 4));
	lpcCLKHigh();
	lpcCLKLow();

	//LPC cycles 13,14
	syncRead = tarWriteToRead();

	if (!syncRead)
		goto skip_sync_at_lpc_write;

	//LPC cycles 15
	lpcCLKHigh();
	if ((syncRead = lpcNibbleRead()) != 0)
		return -1;

	lpcCLKLow();

 skip_sync_at_lpc_write:
	//LPC cycles 16,17
	tarReadToWrite();

	//Some dummy CLK cycles.
	lpcCLKHigh();
	lpcCLKLow();

	lpcCLKHigh();
	lpcCLKLow();

	return 0;
}

char lpcReadIOByte(unsigned short addr, unsigned char *data)
{
	unsigned char syncRead;
	if(data == NULL)
		return -2;

	//LPC cycles 1,2
	startSequenceWithCYCType(0x0);
	//LPC cycles 3-6
	sendIoAddress(addr);

	//LPC cycles 7-8
	syncRead = tarWriteToRead();

	if (!syncRead)
			goto skip_sync_at_lpc_read;

	//LPC cycles 9
	lpcCLKHigh();
	if ((syncRead = lpcNibbleRead()) != 0)
	{
		*data = 0xff;
		return -1;
	}

	lpcCLKLow();

 skip_sync_at_lpc_read:

	//LPC cycle 10: getting data's lower nibble
	lpcCLKHigh();
	*data = lpcNibbleRead();
	lpcCLKLow();

	//LPC cycle 11: getting data's high nibble
	lpcCLKHigh();
	*data |= (lpcNibbleRead() << 4);
	lpcCLKLow();

	//LPC cycles 12,13
	tarReadToWrite();

	//Some dummy CLK cycles.
	lpcCLKHigh();
	lpcCLKLow();

	lpcCLKHigh();
	lpcCLKLow();

	return 0;
}

char lpcWriteIOByte(unsigned short addr, unsigned char data)
{
	unsigned char syncRead;

	//LPC cycles 1,2
	startSequenceWithCYCType(0x2);

	//LPC cycles 3-6
	sendIoAddress(addr);

	//LPC cycle 7
	lpcNibbleWrite(data);
	lpcCLKHigh();
	lpcCLKLow();

	//LPC cycle 8
	lpcNibbleWrite((data >> 4));
	lpcCLKHigh();
	lpcCLKLow();

	//LPC cycles 9,10
	syncRead = tarWriteToRead();

	if (!syncRead)
		goto skip_sync_at_lpc_write;

	//LPC cycles 11
	lpcCLKHigh();
	if ((syncRead = lpcNibbleRead()) != 0)
		return -1;

	lpcCLKLow();

 skip_sync_at_lpc_write:
	//LPC cycles 12,13
	tarReadToWrite();

	//Some dummy CLK cycles.
	lpcCLKHigh();
	lpcCLKLow();

	lpcCLKHigh();
	lpcCLKLow();

	return 0;
}

void LPCInit(void)
{
	//Set Modchip in RST state
	lpcRSTLow();
	//Clock low state
	lpcCLKLow();

	lpcLADOutput();
	//Set LAD port to 0xF(idle state)
	lpcNibbleWrite(0xF);
}
