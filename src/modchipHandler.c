/*
 * modchipHandler.c
 *
 *  Created on: 2015-09-01
 *      Author: benjaminfd
 */

#include "modchipHandler.h"
#include "lpcHandler.h"
#include "gpioHandler.h"
#include "BootLCD.h"

#include <string.h>
#include <stdint.h>
#include <stddef.h>

unsigned short fHasHardware;

KNOWN_FLASH_TYPE currentFlashDevice;

accessModchipSequence modchipDetectRoutine(systemModeState globalModeSet)
{
	accessModchipSequence returnValue;

   //Drop LPC-RST signal and generate ~20 clock cycle on LPC-CLK.
   putDeviceInRSTState(true);
   dummyClockCycles(20);

   //Raise LPC-RST and generate ~20 clock cycle on LPC-CLK.`
   putDeviceInRSTState(false);
   dummyClockCycles(20);

   //Check D0 line in case a XBlast modchip is detected. Should read '1' at this point.
   unsigned char d0State = 0;//getGPIOPinInputState(modchipD0);

   //Start Read flash ID.
   //Check if at least flash chip is detected.
   if(readLPCFlashID(&currentFlashDevice))
   {
	   //report unknown/undetected device.
	   returnValue = stopBlinkingBusyLed;
	   //move directly to 'stopBlinkingBusyLed'.
   }
   else
   {

	   modchipDetected = true;
	   //If flash ID is known, move on to next step.
	   returnValue = globalModeSet == WriteModchipMode ? EraseModchipFlash : ReadVerifyModchipFlash;

	   if(d0State != 0)
	   {
		   //Check D0 line in case a  XBlast modchip is detected. Should read '0' at this point.
		   d0State = getGPIOPinInputState(modchipD0);
	   }
	   unsigned char tempSYSCONID;
	   if(currentFlashDevice.m_bManufacturerId == 0xBF && currentFlashDevice.m_bDeviceId == 0x5B
			   && !readXboxModchipSYSCONID(&tempSYSCONID))
	   {
		   if(d0State && tempSYSCONID == 0x15)
		   {
			   //XBlast Lite V1 successfully detected
			   fHasHardware = 0x15;	//TODO: replace by define macro
			   setGPIOPinOutputState(xblastLed, false);
			   //Force device size to only cover OS bank size (256KB)
			   currentFlashDevice.m_dwLengthInBytes = 0x40000;
			   assertInitLCD();
		   }
		   else if(tempSYSCONID == 0x15)
		   {
			   //Aladdin XT successfully detected
			   fHasHardware = 0x11;	//TODO: replace by define macro
			   setGPIOPinOutputState(xblastLed, false);
			   //Force device size to only cover OS bank size (256KB)
			   currentFlashDevice.m_dwLengthInBytes = 0x40000;
		   }
	   }
	   else
	   {
		   fHasHardware = 0;	//Unknown hardware but flash device is valid
	   }
   }
   return returnValue;
}

unsigned int getDetectedModchipFlashSize()
{
	if(!modchipDetected)
	{
		return 0;
	}

	return currentFlashDevice.m_dwLengthInBytes;
}

unsigned char modchipReadRoutine(unsigned int startAddr, unsigned char *dataBuf, unsigned int dataBufLength)
{
	//sanity check first
	if(startAddr > 0xFFFFF || dataBuf == NULL || dataBufLength == 0)
	{
		return 1;
	}

	for(unsigned int incrPtr = 0; incrPtr < dataBufLength; incrPtr++)
	{
		dataBuf[incrPtr] = readLPCByte(startAddr + incrPtr);
	}

	return 0;
}

accessModchipSequence modchipEraseRoutine(unsigned int startAddr, unsigned int length)
{
	unsigned char eraseOperationResultStatus = 0;
	//0 is good,
	//1 is bad start address,
	//2 is bad erase length,
	//3 is unsupported command.

	//If detected Flash Chip is of 256KB in size, try chip erase to speed up things
	if(currentFlashDevice.m_dwLengthInBytes == 0x40000 && fHasHardware == 0)
	{
		if(!chipEraseAttempt())	//Only 2 possible outcome of this is success or command unsupported.
		{
			return WriteReadVerifyModchipFlash;			//Chip erase successful. Flash device is now blank. Returning.
		}

	}

	//Sanity check on erase range after attempting chip erase in case values are dummy.
	if(startAddr % (4 * 1024) || length < (4 *1024))
	{
		return stopBlinkingBusyLed;
	}

	//Start by trying Block-Erase (64KB)
	eraseOperationResultStatus = blockEraseAttempt(startAddr, length);

	if(eraseOperationResultStatus)	//error in block erase command, let's try sector erase in case this one will work.
	{
		eraseOperationResultStatus = sectorEraseAttempt(startAddr, length);
	}

	if(eraseOperationResultStatus == 1 || eraseOperationResultStatus == 2)	//Start addr or length to erase error.
	{
		return stopBlinkingBusyLed;
	}


	return WriteReadVerifyModchipFlash;
}

unsigned char modchipWriteRoutine(unsigned int startAddr, unsigned char *dataBuf, unsigned int dataBufLength)
{
	//sanity check first
	if(startAddr > 0xFFFFF || dataBuf == NULL || dataBufLength == 0)
	{
		return 1;
	}

	for(unsigned int incrPtr = 0; incrPtr < dataBufLength; incrPtr++)
	{
		if(writeLPCByte(startAddr + incrPtr, dataBuf[incrPtr]))
		{
			return 2;
		}
	}

	return 0;
}

unsigned char modchipVerifyRoutine(unsigned int startAddr, unsigned char *dataBuf, unsigned int dataBufLength)
{
	unsigned char readBuf[dataBufLength];

	for(unsigned int incrPtr = 0; incrPtr < dataBufLength; incrPtr++)
	{
		readBuf[incrPtr] = readLPCByte(startAddr + incrPtr);
	}

	return memcmp(dataBuf, readBuf, dataBufLength);
}
