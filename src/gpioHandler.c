/*
 * gpioHandler.c
 *
 *  Created on: 2015-07-31
 *      Author: benjaminfd
 */

#include "inc/hw_gpio.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "inc/hw_types.h"
#include "gpioHandler.h"

/*
 * GPIO config for whole board is as follow:
 *
 * PA4 : Modchip "D0" signal test (optionnal)
 *
 * User interaction
 * PA2 : XBLAST LED (out, pull-up)
 * PA3 : READ LED (out, pull-up)
 * PA6 : BUSY LED (out, pull-up)
 * PA7 : WRITE LED (out, pull-up)
 * PF0 : Onboard SW2 tactile switch (in, unknown)
 * PF1 : Onboard Red LED (out, pull up)
 * PF2 : Onboard Blue LED (out, pull up)
 * PF3 : Onboard Green LED (out, pull up)
 * PF4 : Onboard SW1 tactile switch (in, unknown)
 *
 * SPI flash interconnects
 * SPI is configured elsewhere
 * PB4 : *SPI2* CLK signal
 * PB5 : *SPI2* CS signal
 * PB6 : *SPI2* MISO signal
 * PB7 : *SPI2* MOSI signal
 *
 * LPC port interconnects
 * No LFRAME signal as this is for Xbox modchips.
 * PD0 : LPC LAD0 (In/Out, pull-up)
 * PD1 : LPC LAD1 (In/Out, pull-up)
 * PD2 : LPC LAD2 (In/Out, pull-up)
 * PD3 : LPC LAD3 (In/Out, pull-up)
 *
 * PE1 : LPC RST (Out, pull-up)
 * PE4 : LPC CLK (Out, pull-up)
 */

const gpioPinConfig redLed = { SYSCTL_PERIPH_GPIOF, GPIO_PORTF_BASE, LED_RED };
const gpioPinConfig blueLed = { SYSCTL_PERIPH_GPIOF, GPIO_PORTF_BASE, LED_BLUE };
const gpioPinConfig greenLed =
		{ SYSCTL_PERIPH_GPIOF, GPIO_PORTF_BASE, LED_GREEN };
const gpioPinConfig onboardSW1 = { SYSCTL_PERIPH_GPIOF, GPIO_PORTF_BASE,
		ONBOARD_SW1 };
const gpioPinConfig onboardSW2 = { SYSCTL_PERIPH_GPIOF, GPIO_PORTF_BASE,
		ONBOARD_SW2 };

const gpioPinConfig xblastLed = { SYSCTL_PERIPH_GPIOA, GPIO_PORTA_BASE,
		XBLAST_LED };
const gpioPinConfig readLed = { SYSCTL_PERIPH_GPIOA, GPIO_PORTA_BASE, READ_LED };
const gpioPinConfig busyLed = { SYSCTL_PERIPH_GPIOA, GPIO_PORTA_BASE, BUSY_LED };
const gpioPinConfig writeLed =
		{ SYSCTL_PERIPH_GPIOA, GPIO_PORTA_BASE, WRITE_LED };

const gpioPinConfig modchipD0 = { SYSCTL_PERIPH_GPIOA, GPIO_PORTA_BASE,
		MODCHIP_D0 };

const gpioPinConfig spi2Clk = { SYSCTL_PERIPH_GPIOB, GPIO_PORTB_BASE, SPI2_CLK };
const gpioPinConfig spi2Cs = { SYSCTL_PERIPH_GPIOB, GPIO_PORTB_BASE, SPI2_CS };
const gpioPinConfig spi2Miso =
		{ SYSCTL_PERIPH_GPIOB, GPIO_PORTB_BASE, SPI2_MISO };
const gpioPinConfig spi2Mosi =
		{ SYSCTL_PERIPH_GPIOB, GPIO_PORTB_BASE, SPI2_MOSI };

const gpioPinConfig lpcLAD0 = { SYSCTL_PERIPH_GPIOD, GPIO_PORTD_BASE, LPC_LAD0 };
const gpioPinConfig lpcLAD1 = { SYSCTL_PERIPH_GPIOD, GPIO_PORTD_BASE, LPC_LAD1 };
const gpioPinConfig lpcLAD2 = { SYSCTL_PERIPH_GPIOD, GPIO_PORTD_BASE, LPC_LAD2 };
const gpioPinConfig lpcLAD3 = { SYSCTL_PERIPH_GPIOD, GPIO_PORTD_BASE, LPC_LAD3 };

const gpioPinConfig lpcRST = { SYSCTL_PERIPH_GPIOE, GPIO_PORTE_BASE, LPC_RST };
const gpioPinConfig lpcCLK = { SYSCTL_PERIPH_GPIOE, GPIO_PORTE_BASE, LPC_CLK };

void gpioSetup(void) {
	//Setup LAD data pins as input at launch.
#define NumberOfInputs 2
	const gpioPinConfig* gpioInputList[NumberOfInputs] = { &modchipD0,
			&spi2Miso};

#define NumberOfIOs 4
	const gpioPinConfig* gpioIOList[NumberOfIOs] = { &lpcLAD0, &lpcLAD1, &lpcLAD2, &lpcLAD3};

#define NumberOfOutputs 12
	const gpioPinConfig* gpioOutputList[NumberOfOutputs] = { &redLed, &blueLed,
			&greenLed, &xblastLed, &readLed, &busyLed, &writeLed, &spi2Clk,
			&spi2Cs, &spi2Mosi, &lpcRST, &lpcCLK };

	unsigned char i;

	//Onboard switches are special
	//
	// Enable the GPIO port to which the pushbuttons are connected.
	//
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);

	//
	// Unlock PF0 so we can change it to a GPIO input
	// Once we have enabled (unlocked) the commit register then re-lock it
	// to prevent further changes.  PF0 is muxed with NMI thus a special case.
	//
	HWREG(GPIO_PORTF_BASE + GPIO_O_LOCK) = GPIO_LOCK_KEY;
	HWREG(GPIO_PORTF_BASE + GPIO_O_CR) |= 0x01;
	HWREG(GPIO_PORTF_BASE + GPIO_O_LOCK) = 0;

	//
	// Set each of the button GPIO pins as an input with a pull-up.
	//
	ROM_GPIODirModeSet(GPIO_PORTF_BASE, ONBOARD_SW1 | ONBOARD_SW2,
			GPIO_DIR_MODE_IN);
	MAP_GPIOPadConfigSet(GPIO_PORTF_BASE, ONBOARD_SW1 | ONBOARD_SW2,
	GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);

	for (i = 0; i < NumberOfInputs; i++) {
		MAP_SysCtlPeripheralEnable(gpioInputList[i]->PeriphBase);
		MAP_SysCtlDelay(3);
		MAP_GPIODirModeSet(gpioInputList[i]->PortBase, gpioInputList[i]->pin,
				GPIO_DIR_MODE_IN);

		MAP_GPIOPadConfigSet(gpioInputList[i]->PortBase, gpioInputList[i]->pin,
				GPIO_STRENGTH_8MA, GPIO_PIN_TYPE_STD); //GPIO_PIN_TYPE_STD_WPU);
	}

	for (i = 0; i < NumberOfIOs; i++) {
		MAP_SysCtlPeripheralEnable(gpioIOList[i]->PeriphBase);
		MAP_SysCtlDelay(3);
		MAP_GPIODirModeSet(gpioIOList[i]->PortBase, gpioIOList[i]->pin,
				GPIO_DIR_MODE_IN);

		MAP_GPIOPadConfigSet(gpioIOList[i]->PortBase, gpioIOList[i]->pin,
				GPIO_STRENGTH_8MA, GPIO_PIN_TYPE_STD_WPD); //GPIO_PIN_TYPE_STD_WPU);
	}

	for (i = 0; i < NumberOfOutputs; i++) {
		MAP_SysCtlPeripheralEnable(gpioOutputList[i]->PeriphBase);
		MAP_SysCtlDelay(3);
		MAP_GPIODirModeSet(gpioOutputList[i]->PortBase, gpioOutputList[i]->pin,
				GPIO_DIR_MODE_OUT);
		MAP_GPIOPadConfigSet(gpioOutputList[i]->PortBase,
				gpioOutputList[i]->pin, GPIO_STRENGTH_8MA,
				GPIO_PIN_TYPE_STD_WPU);
	}

	setGPIOPinOutputState(xblastLed, true);
	setGPIOPinOutputState(readLed, true);
	setGPIOPinOutputState(writeLed, true);
	setGPIOPinOutputState(busyLed, true);
}
void updateOnboardRGBLEDs(void) {
	const gpioPinConfig RGBOnboardLEDs[3] = { redLed, greenLed, blueLed };
	static unsigned char ledCycler = 0;

	//Sets all LEDs off.
	//for(i = 0; i < 3; i++)
	//{
	//	MAP_GPIOPinWrite(RGBOnboardLEDs[i].PortBase, RGBOnboardLEDs[i].pin, 0);
	//}

	//Prepare for next LED.
	ledCycler = (ledCycler + 1) % 3;

	//Set a single LED ON.
	MAP_GPIOPinWrite(RGBOnboardLEDs[ledCycler].PortBase,
			RGBOnboardLEDs[0].pin | RGBOnboardLEDs[1].pin
					| RGBOnboardLEDs[2].pin, RGBOnboardLEDs[ledCycler].pin);

}

bool readSW1State(void) {
	unsigned int returnValue = (MAP_GPIOPinRead(GPIO_PORTF_BASE, ONBOARD_SW1)); // & onboardSW1.pin);
	return returnValue;
}

bool readSW2State(void) {
	unsigned int returnValue = (GPIOPinRead(GPIO_PORTF_BASE, ONBOARD_SW2)); // & onboardSW2.pin);
	return returnValue;
}

void toggleBusyLed(void) {
	unsigned char currentLedState = MAP_GPIOPinRead(busyLed.PortBase,
			busyLed.pin);
	setGPIOPinOutputState(busyLed, !currentLedState);
}

void setGPIOPinDir(gpioPinConfig pin, bool isInput) {
	MAP_GPIODirModeSet(pin.PortBase, pin.pin,
			isInput ? GPIO_DIR_MODE_IN : GPIO_DIR_MODE_OUT);
}

void setGPIOPortDir(gpioPinConfig pin, char pinMask, bool isInput) {
	MAP_GPIODirModeSet(pin.PortBase, pinMask,
			isInput ? GPIO_DIR_MODE_IN : GPIO_DIR_MODE_OUT);
}

void setGPIOPinOutputState(gpioPinConfig pin, bool state) {
	MAP_GPIOPinWrite(pin.PortBase, pin.pin, state ? pin.pin : 0);
}

void setGPIOPortOutputState(gpioPinConfig pin, unsigned char mask,
		unsigned char state) {
	MAP_GPIOPinWrite(pin.PortBase, mask, state);
}

bool getGPIOPinInputState(gpioPinConfig pin) {
	return (bool) (MAP_GPIOPinRead(pin.PortBase, pin.pin) & pin.pin);
}

char getGPIOPortInputState(gpioPinConfig pin) {
	return MAP_GPIOPinRead(pin.PortBase, 0xFF);
}
