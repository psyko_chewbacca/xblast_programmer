#include <stdint.h>
#include <string.h>
#include "BootLCD.h"
#include "lpc_io.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"

#include <stdlib.h>

typedef struct _xLCD {
	int DisplayType;
	int enable;
	int LineSize;
	int nbLines;
	int TimingCMD;
	int TimingData;

	char lineWriteInProgress;
	unsigned char segmentedWriteCharacterPosition;

	char *currentLineBufferString;

	unsigned char Line1Start;
	unsigned char Line2Start;
	unsigned char Line3Start;
	unsigned char Line4Start;

	void (*Init)(void);
	void (*Command)(unsigned char value);
	void (*Data)(unsigned char value);

	void (*WriteIO)(unsigned char data, bool RS);

	void (*PrintLine[4])(bool centered, char *lineText);

	void (*ClearLine)(unsigned char line);
} _xLCD;    //Will be know as xLCD from now on.

_xLCD xLCD;


void BootLCDInit(void) {
	xLCD.enable = 0;            //Set it unintialized for now.
	xLCD.TimingCMD = 1500;      //Arbitrary but safe.
	xLCD.TimingData = 90;

	BootLCDSwitchType();

	//Function pointers included in struct for easier access throughout the program.
	xLCD.Init = WriteLCDInit;
	xLCD.Command = WriteLCDCommand;
	xLCD.Data = WriteLCDData;
	xLCD.WriteIO = WriteLCDIO;
	xLCD.PrintLine[0] = WriteLCDLine0;
	xLCD.PrintLine[1] = WriteLCDLine1;
	xLCD.PrintLine[2] = WriteLCDLine2;
	xLCD.PrintLine[3] = WriteLCDLine3;
	xLCD.ClearLine = WriteLCDClearLine;
}

void BootLCDSwitchType(void) {
	xLCD.LineSize = 20;    //Defaults to 4 lines LCDs
	xLCD.nbLines = 4;        //Defaults to 20 chars/line
	xLCD.Line1Start = 0x00;    //4 lines config is good for smaller LCD too.
	xLCD.Line2Start = 0x40;
	xLCD.Line3Start = xLCD.Line1Start + xLCD.LineSize;
	xLCD.Line4Start = xLCD.Line2Start + xLCD.LineSize;

	if(xLCD.currentLineBufferString != NULL){
		free(xLCD.currentLineBufferString);
	}

	xLCD.currentLineBufferString = (char *)malloc(xLCD.LineSize + 1);

	xLCD.lineWriteInProgress = -1;
	xLCD.segmentedWriteCharacterPosition = -1;
}

void setLCDContrast(unsigned char value) {
	float fBackLight = ((float) value) / 100.0f;
	fBackLight *= 127.0f;
	unsigned char newValue = (unsigned char) fBackLight;
	if (newValue == 63)
		newValue = 64;
	lpcWriteIOByte(LCD_CT, newValue);
}

void setLCDBacklight(unsigned char value) {
	float fBackLight = ((float) value) / 100.0f;
	fBackLight *= 127.0f;
	unsigned char newValue = (unsigned char) fBackLight;
	if (newValue == 63)
		newValue = 64;
	lpcWriteIOByte(LCD_BL, newValue);
}

void assertInitLCD(void) {
	if (xLCD.enable != 1) {    //Display should be ON but is not initialized.

		xLCD.enable = 1;
		setLCDContrast(20);
		setLCDBacklight(85);
		MAP_SysCtlDelay(100); //Wait a precautionary 10ms before initializing the LCD to let power stabilize.
		WriteLCDInit();
		BootLCDSwitchType();
		initialLCDPrint();
	}

}

void WriteLCDInit(void) {
	if (xLCD.enable != 1)
		return;

	//It's been at least ~15ms since boot.
	//Start of init, with delay
	xLCD.WriteIO(0x33, 0); //Use a single call to write twice function set 0b0011 with 4.1ms delay

	xLCD.WriteIO(0x32, 0); //Again a single call to write but this time write 0b0011 in first and 0b0010 in second
	//Second write could be shorter but meh...

	//LCD is now in 4-bit mode.
	MAP_SysCtlDelay(1);
	xLCD.Command(DISP_FUNCTION_SET | DISP_N_FLAG | DISP_RE_FLAG); //2 lines and 5x8 dots character resolution.
	MAP_SysCtlDelay(1);
	xLCD.Command(DISP_SEGRAM_SET);  //Display OFF, Cursor OFF, Cursor blink OFF.
	MAP_SysCtlDelay(1);
	xLCD.Command(DISP_EXT_CONTROL | DISP_NW_FLAG);
	MAP_SysCtlDelay(1);
	xLCD.Command(DISP_FUNCTION_SET | DISP_N_FLAG); //Entry mode,Increment cursor, shift right
	MAP_SysCtlDelay(1);
	xLCD.Command(DISP_CONTROL | DISP_D_FLAG);        //Display ON.
	MAP_SysCtlDelay(1);
	xLCD.Command(DISP_CLEAR);
	MAP_SysCtlDelay(1);
	xLCD.Command(DISP_ENTRY_MODE_SET | DISP_ID_FLAG);
	MAP_SysCtlDelay(1);
	xLCD.Command(DISP_HOME);

}

void WriteLCDCommand(unsigned char value) {
	if (xLCD.enable != 1)
		return;
	xLCD.WriteIO(value, 0);        //RS=0 for commands.
}

void WriteLCDData(unsigned char value) {
	if (xLCD.enable != 1)
		return;
	xLCD.WriteIO(value, DISPLAY_RS);
}

void WriteLCDIO(unsigned char data, bool RS) {
	unsigned char lsbNibble = 0;
	unsigned char msbNibble = 0;

	if (xLCD.enable != 1)
		return;
	//data is b7,b6,b5,b4,b3,b2,b1,b0
	msbNibble = ((data >> 2) & 0x28) | RS;   //Maps     0,b6,b7,b4,b5,E,RS,0
	msbNibble |= (data >> 0) & 0x50;
	lsbNibble = ((data << 2) & 0x28) | RS;   //Maps     0,b2,b3,b0,b1,E,RS,0
	lsbNibble |= (data << 4) & 0x50;         //data must be x,D7,D6,D5,D4,E,RS,x
	//E signal value is added below as it's the "clock"
	//High nibble first
	//Initially place the data
	lpcWriteIOByte(LCD_DATA, msbNibble); //Place bit7,bit6,bit5,bit4,E,RS,x
	MAP_SysCtlDelay(5);    //needs to be at least 40ns

	msbNibble |= DISPLAY_E;
	//Raise E signal line
	lpcWriteIOByte(LCD_DATA, msbNibble); //Place bit7,bit6,bit5,bit4,E,RS,x
	MAP_SysCtlDelay(20);    //needs to be at least 230ns

	msbNibble ^= DISPLAY_E;
	//Drop E signal line
	lpcWriteIOByte(LCD_DATA, msbNibble); //Place bit7,bit6,bit5,bit4,E,RS,x
	MAP_SysCtlDelay(1000);

	//Low nibble in second
	//Initially place the data
	lpcWriteIOByte(LCD_DATA, lsbNibble); //Place bit3,bit2,bit1,bit0,E,RS,x
	MAP_SysCtlDelay(5);    //needs to be at least 40ns

	lsbNibble |= DISPLAY_E;
	//Raise E signal line
	lpcWriteIOByte(LCD_DATA, lsbNibble); //Place bit3,bit2,bit1,bit0,E,RS,x
	MAP_SysCtlDelay(20);    //needs to be at least 230ns

	lsbNibble ^= DISPLAY_E;
	//Drop E signal line
	lpcWriteIOByte(LCD_DATA, lsbNibble); //Place bit3,bit2,bit1,bit0,E,RS,x
	MAP_SysCtlDelay(1000);
}

void WriteLCDLine0(bool centered, char *lineText) {
	char LineBuffer[xLCD.LineSize];    //For the escape character at the end.

	if (xLCD.enable != 1)
		return;

	if(xLCD.lineWriteInProgress >= 0 && xLCD.lineWriteInProgress != 0)
		return;

	if(xLCD.lineWriteInProgress == -1)
	{
		xLCD.lineWriteInProgress = 0;
		xLCD.segmentedWriteCharacterPosition = 0;

		if (centered) {
			//Play with the string to center it on the LCD unit.
			WriteLCDCenterString(LineBuffer, lineText);
		} else
			WriteLCDFitString(LineBuffer, lineText);

		//Place cursor
		WriteLCDSetPos(0, 0);    // Write to first line
	}
}

void WriteLCDLine1(bool centered, char *lineText) {
	char LineBuffer[xLCD.LineSize];    //For the escape character at the end.

	if (xLCD.enable != 1 || xLCD.nbLines <= 1)
		return;

	if(xLCD.lineWriteInProgress >= 0 && xLCD.lineWriteInProgress != 1)
		return;

	if(xLCD.lineWriteInProgress == -1)
	{
		xLCD.lineWriteInProgress = 1;
		xLCD.segmentedWriteCharacterPosition = 0;

		if (centered) {
			//Play with the string to center it on the LCD unit.
			WriteLCDCenterString(LineBuffer, lineText);
		} else
			WriteLCDFitString(LineBuffer, lineText);

		//Place cursor
		WriteLCDSetPos(0, 1);    // Write to second line
	}
}

void WriteLCDLine2(bool centered, char *lineText) {
	char LineBuffer[xLCD.LineSize];    //For the escape character at the end.

	if (xLCD.enable != 1 || xLCD.nbLines <= 2)
		return;

	if(xLCD.lineWriteInProgress >= 0 && xLCD.lineWriteInProgress != 2)
		return;

	if(xLCD.lineWriteInProgress == -1)
	{
		xLCD.lineWriteInProgress = 2;
		xLCD.segmentedWriteCharacterPosition = 0;

		if (centered) {
			//Play with the string to center it on the LCD unit.
			WriteLCDCenterString(LineBuffer, lineText);
		} else
			WriteLCDFitString(LineBuffer, lineText);

		//Place cursor
		WriteLCDSetPos(0, 2);    // Write to third line
	}
}

void WriteLCDLine3(bool centered, char *lineText) {
	char LineBuffer[xLCD.LineSize];    //For the escape character at the end.

	if (xLCD.enable != 1 || xLCD.nbLines <= 2)
		return;

	if(xLCD.lineWriteInProgress >= 0 && xLCD.lineWriteInProgress != 3)
		return;

	if(xLCD.lineWriteInProgress == -1)
	{
		xLCD.lineWriteInProgress = 3;
		xLCD.segmentedWriteCharacterPosition = 0;

		if (centered) {
			//Play with the string to center it on the LCD unit.
			WriteLCDCenterString(LineBuffer, lineText);
		} else
			WriteLCDFitString(LineBuffer, lineText);

		//Place cursor
		WriteLCDSetPos(0, 3);    // Write to fourth line
	}
}

void WriteLCDCenterString(char * StringOut, char * stringIn) {
	int i;
	memset(StringOut, 0x0, xLCD.LineSize);    //Let's get clean a little.

	if (xLCD.enable != 1)
		return;

	//Skip first character.
	if (stringIn[0] == '\2') {
		strncpy(StringOut, &stringIn[1], xLCD.LineSize - 1); //We skipped the first character.
	} else {
		strncpy(StringOut, stringIn, xLCD.LineSize);   //Line length is variable
	}
	StringOut[xLCD.LineSize] = 0;  //Escape character at the end that's for sure
	i = strlen(StringOut);
	//String length is shorter than what can be displayed on a single line of the LCD unit.
	if (i < xLCD.LineSize) {
		char szTemp1[xLCD.LineSize];
		unsigned char rest = (xLCD.LineSize - i) / 2;

		//Print "space"(0x20 in ascii) in the whole array.
		memset(szTemp1, 0x20, xLCD.LineSize);
		memcpy(&szTemp1[rest], StringOut, i); //Place actual text in the middle of the array.
		memcpy(StringOut, szTemp1, xLCD.LineSize);
		//LineBuffer now contains our text, centered on a single LCD unit's line.
	}
}

void WriteLCDFitString(char * StringOut, char * stringIn) {
	int i;
	memset(StringOut, 0x0, xLCD.LineSize);    //Let's get clean a little.

	if (xLCD.enable != 1)
		return;

	//Skip first character.
	if (stringIn[0] == '\2') {
		strncpy(StringOut, &stringIn[1], xLCD.LineSize - 1); //We skipped the first character.
	} else {
		strncpy(StringOut, stringIn, xLCD.LineSize);   //Line length is variable
	}
	StringOut[xLCD.LineSize] = 0;  //Escape character at the end that's for sure
	i = strlen(StringOut);
	//String length is shorter than what can be displayed on a single line of the LCD unit.
	if (i < xLCD.LineSize) {
		char szTemp1[xLCD.LineSize];

		//Print "space"(0x20 in ascii) in the whole array.
		memset(szTemp1, 0x20, xLCD.LineSize);
		memcpy(szTemp1, StringOut, i); //Place actual text justified to the left.
		memcpy(StringOut, szTemp1, xLCD.LineSize);
		//LineBuffer now contains our text, centered on a single LCD unit's line.
	}
}

void WriteLCDSetPos(unsigned char pos, unsigned char line) {
	unsigned char cursorPtr = pos % xLCD.LineSize;

	if (xLCD.enable != 1)
		return;

	if (line == 0) {
		cursorPtr += xLCD.Line1Start;
	}
	if (line == 1) {
		cursorPtr += xLCD.Line2Start;
	}
	if (line == 2) {
		cursorPtr += xLCD.Line3Start;
	}
	if (line == 3) {
		cursorPtr += xLCD.Line4Start;
	}

	xLCD.Command(DISP_DDRAM_SET | cursorPtr);

}

void WriteLCDClearLine(unsigned char line) {
	char empty[xLCD.LineSize];

	if (xLCD.enable != 1)
		return;

	memset(empty, ' ', xLCD.LineSize);

	//Call the proper function for the desired line.
	xLCD.PrintLine[line](JUSTIFYLEFT, empty);
}

void initialLCDPrint(void) {
	xLCD.Command(DISP_CLEAR);
	xLCD.PrintLine[0](CENTERSTRING, "XBlast mod V1");
}

void updateLCD(void) {
	if(xLCD.lineWriteInProgress >= 0)
	{
		xLCD.Data(xLCD.currentLineBufferString[xLCD.segmentedWriteCharacterPosition++]);

		if(xLCD.currentLineBufferString[xLCD.segmentedWriteCharacterPosition] == '\0' || xLCD.segmentedWriteCharacterPosition == xLCD.LineSize)
		{
			xLCD.lineWriteInProgress = -1;
			xLCD.segmentedWriteCharacterPosition = 0;
		}
	}
}
