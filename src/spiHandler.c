/*
 * spiHandler.c
 *
 *  Created on: 2015-09-02
 *      Author: benjaminfd
 */


#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/pin_map.h"

#include <stdint.h>
#include <stdbool.h>
#include "gpioHandler.h"
#include "driverlib/ssi.h"
#include "spi_flash.h"
#include <string.h>

void spiSetup(void)
{
	//Assume GPIOs of target SSI port are already configured and GPIO periph is enabled

	//Hardcoded for now as we will only use this SSI peripheral for now.
	MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI2);
	MAP_SysCtlPeripheralReset(SYSCTL_PERIPH_SSI2);

	MAP_SSIDisable(SSI2_BASE);

	MAP_SSIClockSourceSet(SSI2_BASE, SSI_CLOCK_SYSTEM);

	GPIOPinConfigure(GPIO_PB4_SSI2CLK);
	GPIOPinConfigure(GPIO_PB5_SSI2FSS);
	GPIOPinConfigure(GPIO_PB6_SSI2RX);
	GPIOPinConfigure(GPIO_PB7_SSI2TX);

	//GPIOPinTypeSSI(spi2Clk.PortBase, spi2Clk.pin | spi2Cs.pin | spi2Miso.pin | spi2Mosi.pin);

	SPIFlashInit(SSI2_BASE, SysCtlClockGet(), 8000000);	//Hardcoded at 8MHz
}

void initSPIFlash(void)
{

}

unsigned char writeSPIFlash(unsigned int addr, unsigned char *data, unsigned int bufLength)
{
	//SPIFlashPageProgram(SSI2_BASE, addr, data, bufLength);
	return 0;
}

unsigned char readSPIFlash(unsigned int addr, unsigned char *data, unsigned int bufLength)
{
	//SPIFlashRead(SSI2_BASE, addr, data, bufLength);
	return 0;
}

unsigned char verifySPIFlash(unsigned int addr,unsigned char *data, unsigned int bufLength)
{
	unsigned char verifBuffer[16];

	for(unsigned int i = 0; i < bufLength; i += 16)
	{
		//SPIFlashRead(SSI2_BASE, addr + i, verifBuffer, 16);

		if(memcmp(data + i, verifBuffer, 16))
		{
			return 1;
		}
	}

	return 0;
}
